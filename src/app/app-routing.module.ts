import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './dashboard/services/auth.guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: '', component: LoginComponent},
  { path: '*/*/*/*/*', redirectTo: ''}
];

@NgModule({
  imports: [
      RouterModule.forRoot(
        routes, { useHash: true }
      )
  ],
  exports: [
      RouterModule
  ]
})

export class AppRoutingModule { }
