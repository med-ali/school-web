import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { PagerService } from  '../../dashboard/services/pager.service'; 
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../dashboard/services/search.service';
import { Subject } from '../../dashboard/models/subject.model';
import { SubjectService } from '../../dashboard/services/subject.service';
import { UserService } from '../../dashboard/services/user.service';
//import { ITEMS_PER_PAGE, Principal } from '../../shared';
//import { Supplier, SupplierService } from '../supplier';
/*import { NewSubjectComponent } from './class-dialog.component';
import { DeleteClassComponent } from './delete-class.component';*/
import { MessageService } from '../../dashboard/services/message.service';
@Component({
    selector: 'jhi-class',
    templateUrl: './subject.component.html'
})
export class SubjectComponent implements OnInit, OnDestroy {

  totalItems: any;
  queryCount: any;
  page: any;
  subjects:Subject[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  compTitle:number = 1
  compDescription:number = 1
  subscription: Subscription;
  message: any;
  sortTitle:boolean = true;
  sortDesc:boolean = true;
   userRole:any;
    constructor(private MessageService: MessageService,private activatedRoute: ActivatedRoute,
      private _searchService: SearchService,private modalService: NgbModal, 
      private subjectService: SubjectService ,private userService: UserService, 
      private pagerService: PagerService,private route: ActivatedRoute,private router: Router,
        ) {  
        this.subscription = this.MessageService.getMessage().subscribe(message => { 
          console.log("message"+message["text"])
          if (message["text"] == "subject added" || message["text"] == "subject edited")  
          {
            this.getAll()
            this.message = message;
          }
        });   
    }
    ngOnInit() {
        this.getAll()
        this.userRole = localStorage.getItem('userRole')
    }
 
    ngOnDestroy() {
        
    }
    loadAll(){
      this.subjectService.findSubjectsByEudiantId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => {
        this.subjects = value["body"]["content"];
        this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
            if (this.totalPages>6){
              this.pageView =  6
            }else{
              this.pageView =  this.totalPages
            } 
      })
    }
    getAll(){
        this.currentPage = 0
        this.subjectService.findSubjectsByEudiantId(this.route["snapshot"]["params"]["id"],0).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"];
            console.log(value["body"].length +"this.classes"+this.subjects)
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            if (this.totalPages>6){
              this.pageView =  6
            }else{
              this.pageView =  this.totalPages
            }
            //this.setPage(1);
          }
        );   
    }
    pagedItems: any[];
    TabReturn(length){
      return new Array(length);
    }

  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        this._searchService.querySearch(this.queryField['value'],page).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }
      else{
        this.currentPage = page
        this.subjectService.query(page).subscribe(  
          (value) => { 
            this.subjects= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
}