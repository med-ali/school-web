import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms'; 
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Answer } from '../../dashboard/models/Answer.model';
import { AnswerService } from '../../dashboard/services/answer.service';
import { Proposition } from '../../dashboard/models/Proposition.model';
@Component({
  selector: 'app-edit-answer',
  templateUrl: './edit-answer.component.html',
  styleUrls: ['./edit-answer.component.scss']
})
export class EditAnswerComponent implements OnInit {

  answerForm: FormGroup;
  questionId:number
  answer:Answer
  propositions:Proposition[]
  answerId:number
  userId:number
  constructor(private answerService: AnswerService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private router: Router,private activeModal: NgbActiveModal) { }

  ngOnInit() {  
    this.initForm();
  }

  initForm() { 
    this.answerForm = this.formBuilder.group({ 
      Description: ['', Validators.required]  
    });
  }
  
  refresh(): void {
    window.location.reload();
  }

  onSubmitForm() {
    const formValue = this.answerForm.value;
    const editAnswer = new Answer( 
      formValue['Description'],
      null
    );
    this.answerService.editAnswer(this.userId,this.answerId,editAnswer).subscribe(
      (value) => { 
        this.refresh()  
      },
      (error) => { 
        this.clear()
        this.openError()
      })
  }

  deleteAnswer(answerId:number){
    this.answerService.deleteAnswer(this.answerId).subscribe(
      (value) => { 
        this.refresh()  
      },
      (error) => { 
        this.clear()
        this.openError()
      })
  }
  
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
