import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms'; 
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Answer } from '../../dashboard/models/Answer.model';
import { Proposition } from '../../dashboard/models/Proposition.model';
import { AnswerService } from '../../dashboard/services/answer.service';
import { MessageService } from '../../dashboard/services/message.service';
@Component({
  selector: 'app-new-answer',
  templateUrl: './new-answer.component.html',
  styleUrls: ['./new-answer.component.scss']
})

export class NewAnswerComponent implements OnInit {
  answerForm: FormGroup;
  questionId:number
  questionType:string
  propositions: Proposition[]
  answer:Answer
  userId:number
  secondTimer:number
  minuteTimer:number
  constructor(private MessageService: MessageService,private answerService: AnswerService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private router: Router,private activeModal: NgbActiveModal) { }
  startCountdown(seconds:number){
      var counterMinute = Math.floor(seconds / 60 )
      var counterSecond = seconds; 
      var interval = setInterval(() => { 
        counterSecond--;
        counterMinute = Math.floor(counterSecond  / 60 )
        this.secondTimer = counterSecond % 60
        this.minuteTimer = counterMinute
        if(counterSecond < 0 ){         
          clearInterval(interval);
          this.clear()
        };
    }, 1000);
  };
  ngOnInit() { 
    if(!this.answer){
      this.startCountdown(120) 
    }
    this.userId = Number(localStorage.getItem('userId'))
    this.initForm();
  }

  initForm() { 
    this.answerForm = this.formBuilder.group({ 
      Description: ['', Validators.required]  
    });
  }
  
  refresh(): void {
    window.location.reload();
  }

  deleteAnswer(answerId:number){
    this.answerService.deleteAnswer(answerId).subscribe(
      (value) => { 
        this.refresh()  
      },
      (error) => { 
        this.clear()
        this.openError()
      })
  }

  onSubmitForm() {
    console.log("this.userId")
    console.log(localStorage.getItem('userId'))
    console.log(Number(localStorage.getItem('userId')))
    const formValue = this.answerForm.value;
    const newAnswer = new Answer( 
      formValue['Description'],
      null
    );
    this.answerService.create(newAnswer,this.userId,this.questionId).subscribe(
      (value) => { 
        this.clear()  
        this.MessageService.sendMessage("answer added")
      },
      (error) => { 
        this.clear()
        this.openError()
      })
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
