 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core'; 
import {AnswerRoute  } from './answers.route';
import {AnswerPopupRoute } from './answers.route'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {MatDialogModule} from '@angular/material';  
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { MaterialModule } from '../../shared/shared.module';
import { NewAnswerComponent } from './new-answer.component';
import { EditAnswerComponent } from './edit-answer.component';
import { NewAnswerQcmComponent } from './new-answer-qcm.component';
import { EditQcmAnswerComponent } from './edit-qcm-answer.component';

const ENTITY_STATES = [
    ...AnswerRoute,
    ...AnswerPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule, 
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule, 
        MaterialModule 
    ],
    declarations: [ 
        NewAnswerComponent, EditAnswerComponent, NewAnswerQcmComponent, EditQcmAnswerComponent         
    ], 
    entryComponents: [
        NewAnswerComponent,
        EditAnswerComponent,
        NewAnswerQcmComponent,
        EditQcmAnswerComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AnswersModule { }
 