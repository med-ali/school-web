import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms'; 
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Answer } from '../../dashboard/models/Answer.model';
import { AnswerService } from '../../dashboard/services/answer.service';
import { Proposition } from '../../dashboard/models/Proposition.model';
import { PropositionService } from '../../dashboard/services/proposition.service';
@Component({
  selector: 'app-edit-qcm-answer',
  templateUrl: './edit-qcm-answer.component.html',
  styleUrls: ['./edit-qcm-answer.component.scss']
})
export class EditQcmAnswerComponent implements OnInit {
  propositionForm: FormGroup;
  questionId:number 
  propositions:Proposition[] 
  constructor(private propositionService: PropositionService,private answerService: AnswerService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private router: Router,private activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() { 
    this.propositionForm = this.formBuilder.group({  
    });
  }

  onChange(proposition:Proposition,isChecked: boolean) {
    console.log(proposition) 
    if (isChecked)
    {
      proposition.userAnswer = true
      proposition.questionId = this.questionId
      proposition.userId = 581
      this.propositions.forEach(element => {
      if (element.description == proposition.description)
        {
          proposition.userAnswer = true
          proposition.questionId = this.questionId
          proposition.userId = 581
        }
      });
      //this.propositionsChecked.push(proposition)
    }
    else
    { 
      this.propositions.forEach(element => {
        if (element.description == proposition.description)
          {
            proposition.userAnswer = false
            proposition.questionId = this.questionId
            proposition.userId = 581
          }
      });
      //let index = this.propositionsChecked.indexOf(proposition);
      //this.propositionsChecked.splice(index,1);
    }
     console.log(this.propositions)
  }

  onSubmitForm() {
    this.propositionService.saveUserAnswer(581,this.questionId,this.propositions).subscribe(
      (value) => { 
        this.refresh()  
      },
      (error) => { 
        this.clear()
        this.openError()
      })
  }

  refresh(): void {
    window.location.reload();
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
