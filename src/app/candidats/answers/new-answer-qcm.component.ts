import { Component, OnInit } from '@angular/core'; 
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms'; 
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Answer } from '../../dashboard/models/Answer.model';
import { Proposition } from '../../dashboard/models/Proposition.model';
import { AnswerService } from '../../dashboard/services/answer.service';
import { PropositionService } from '../../dashboard/services/proposition.service';
@Component({
  selector: 'app-new-answer-qcm',
  templateUrl: './new-answer-qcm.component.html',
  styleUrls: ['./new-answer-qcm.component.scss']
})
export class NewAnswerQcmComponent implements OnInit {
  propositionForm: FormGroup;
  questionId:number
  userId:number
  questionType:string
  propositions:Proposition[] 
  answers:Answer[] 
  propositionsChecked:Array<Proposition> = []
  secondTimer:number
  minuteTimer:number
  constructor(private propositionService: PropositionService,private answerService: AnswerService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private router: Router,private activeModal: NgbActiveModal) { }
    startCountdown(seconds:number){
      var counterMinute = Math.floor(seconds / 60 )
      var counterSecond = seconds; 
      var interval = setInterval(() => { 
        counterSecond--;
        counterMinute = Math.floor(counterSecond  / 60 )
        this.secondTimer = counterSecond % 60
        this.minuteTimer = counterMinute
        if(counterSecond < 0 ){         
          clearInterval(interval);
          this.clear()
        };
      }, 1000);
    };
  
  ngOnInit() {
    this.userId = Number(localStorage.getItem('userId'))
    if(this.answers.length == 0){
      this.startCountdown(120)
    } 
    this.propositionService.getAllPropositionByQcmQuestion(this.questionId).subscribe(
      (value) => {  
        this.propositions = value["body"]["content"]
        this.propositions.forEach(element => {          
          element.userAnswer = false
          element.questionId = this.questionId
          element.userId = this.userId            
        });
      }, 
      (error) => {
    })
    this.initForm();
  }

  initForm() { 
    this.propositionForm = this.formBuilder.group({  
    });
  }

  
  onChange(proposition:Proposition,isChecked: boolean) { 
    if (isChecked)
    {
      proposition.userAnswer = true
      proposition.questionId = this.questionId
      proposition.userId = this.userId
      this.propositions.forEach(element => {
      if (element.description == proposition.description)
        {
          proposition.userAnswer = true
          proposition.questionId = this.questionId
          proposition.userId = this.userId
        }
      });
      this.propositionsChecked.push(proposition) 
    }
    else
    { 
      let index = this.propositionsChecked.indexOf(proposition);
      this.propositionsChecked.splice(index,1); 
      this.propositions.forEach(element => {
        if (element.description == proposition.description)
          {
            proposition.userAnswer = false
            proposition.questionId = this.questionId
            proposition.userId = this.userId
          }
      }); 
    }
  }

  onSubmitForm() {
    this.propositionService.saveUserAnswer(this.userId,this.questionId,this.propositionsChecked).subscribe(
      (value) => { 
        //this.refresh()
        this.clear()
      },
      (error) => { 
        this.clear()
        this.openError()
    }) 
  }

  refresh(): void {
    window.location.reload();
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
