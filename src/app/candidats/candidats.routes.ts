import { Routes ,Route} from '@angular/router';
import { RouterModule } from '@angular/router';
import { ExamRoute ,ExamPopupRoute } from './exam-list/exam-list.route';
import { QuestionRoute ,QuestionPopupRoute } from './question-list/question-list.route';
import { AnswerRoute ,AnswerPopupRoute } from './answers/answers.route';
import { SubjectRoute } from './subjects/subject.route';
import { CandidatsComponent } from './candidats.component';
export const MODULE_ROUTES: Route[] = [
  {
    path:'candidats',  component:  CandidatsComponent,children: [ 
      ...ExamRoute ,
      ...QuestionRoute ,
      ...QuestionPopupRoute,
      ...AnswerRoute ,
      ...AnswerPopupRoute,
      ...SubjectRoute
    ]
  }
];