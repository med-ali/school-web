import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'; 
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MODULE_ROUTES } from './candidats.routes';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/shared.module';
import { ExamListComponent } from './exam-list/exam-list.component'; 
import { ExamDetailComponent } from './exam-list/exam-detail.component';
import { QuestionListComponent } from './question-list/question-list.component'; 
import { ExamListModule } from './exam-list/exam-list.module';
import { QuestionListModule } from './question-list/question-list.module';
import { AnswersModule } from './answers/answers.module';
import { SubjectComponent } from './subjects/subject.component';
import { SubjectModule } from './subjects/subject.module';
import { CandidatsComponent } from './candidats.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { NavBarComponent } from '../dashboard/nav-bar/nav-bar.component';
@NgModule({  
  declarations: [ 
    CandidatsComponent
  ],
  entryComponents: [
     
  ],
  imports: [ 
    BrowserModule,
    //AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,   
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forChild(MODULE_ROUTES),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,   
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ExamListModule,
    QuestionListModule,
    AnswersModule,
    SubjectModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
     
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [ ]
})
export class CandidatsModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}