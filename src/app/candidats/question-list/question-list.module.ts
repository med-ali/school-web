 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core'; 
import {QuestionRoute  } from './question-list.route';
import {QuestionPopupRoute } from './question-list.route'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {MatDialogModule} from '@angular/material'; 
import { QuestionListComponent } from './question-list.component'; 
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { MaterialModule } from '../../shared/shared.module';

const ENTITY_STATES = [
    ...QuestionRoute,
    ...QuestionPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule, 
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule, 
        MaterialModule 
    ],
    declarations: [         
        QuestionListComponent   
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QuestionListModule { }
 