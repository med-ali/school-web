import { Component, OnDestroy, OnInit , ViewChild } from '@angular/core';
import { User } from '../../dashboard/models/User.model';
import { Exam } from '../../dashboard/models/Exam.model';
import { Question } from '../../dashboard/models/Question.model';
import { Subscription } from 'rxjs';
import { UserService } from '../../dashboard/services/user.service';
import { ExamService } from '../../dashboard/services/exam.service';
import { PagerService } from  '../../dashboard/services/pager.service'; 
import { QuestionService } from '../../dashboard/services/question.service';
import { PropositionService } from '../../dashboard/services/proposition.service';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../dashboard/services/search.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http'; 
import { NewAnswerComponent } from '../answers/new-answer.component';
import { EditAnswerComponent } from '../answers/edit-answer.component';
import { EditQcmAnswerComponent } from '../answers/edit-qcm-answer.component';
import { AnswerService } from '../../dashboard/services/answer.service';
import { NewAnswerQcmComponent } from '../answers/new-answer-qcm.component';
import { MessageService } from '../../dashboard/services/message.service';
@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  questions:Question[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  examId:number
  userId:number
  queryField: FormControl = new FormControl();
  compTitle:number = 0
  compDescription:number = 0
  message: any;
  subscription: Subscription
  constructor(private MessageService: MessageService,private propositionService: PropositionService,private answerService: AnswerService,private activatedRoute: ActivatedRoute,private questionService: QuestionService,private modalService: NgbModal
    ,private route: ActivatedRoute,private router: Router,
    ) {
      this.userId = Number(localStorage.getItem('userId'))
      this.examId = this.route["snapshot"]["params"]["id"]     
      this.currentPage = 0 
      this.subscription = this.MessageService.getMessage().subscribe(message => { 
        this.getQuestions()
        this.message = message;
       });     
  }

  ngOnInit() {
    this.userId = Number(localStorage.getItem('userId'))
    this.examId = this.route["snapshot"]["params"]["id"]     
    this.currentPage = 0 
    this.getQuestions()
  }
  
  
  TabReturn(length) {
    return new Array(length);
  }

  answer(questionId:number,type:string) { 
    if (type=="qroQuestion"){
      this.answerService.getAnswerByquestionAndUser(this.userId,questionId).subscribe(
        (value) => {      
          const modalRef = this.modalService.open( NewAnswerComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.questionId = questionId;
          if (value["body"][0]){   
            modalRef.componentInstance.answer = value["body"][0];
            modalRef.componentInstance.answerId = value["body"][0]["id"];
            modalRef.componentInstance.questionId = questionId;
            modalRef.componentInstance.questionType = type;
          }   
        },
        (error) => {  
        }) 
    }
    else
    {
      console.log('qcmquestion')
      this.answerService.getAnswerByquestionAndUser(this.userId,questionId).subscribe(
        (value) => {     
          const modalRef = this.modalService.open( NewAnswerQcmComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.questionId = questionId;
          modalRef.componentInstance.questionType = type; 
          if (value["body"]){   
            modalRef.componentInstance.answers = value["body"]; 
          }   
        },
        (error) => {  
      })    
    }
  }
  
  editAnswer(questionId:number,type:string){
    if (type == "qroQuestion"){
      this.answerService.getAnswerByquestionAndUser(this.userId,questionId).subscribe(
        (value) => {      
          const modalRef = this.modalService.open( EditAnswerComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.questionId = questionId;
          if (value["body"][0]){
            console.log(value["body"][0])  
            modalRef.componentInstance.answer = value["body"][0];
            modalRef.componentInstance.answerId = value["body"][0]["id"];
          }   
        },
        (error) => {  
        }) 
    }
    else{
      this.propositionService.getAllPropositionByQcmQuestionByUser(this.userId,questionId).subscribe(
        (value) => {      
          const modalRef = this.modalService.open( EditQcmAnswerComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.questionId = questionId;
          if (value["body"]){   
            console.log(value["body"]["content"])
            modalRef.componentInstance.propositions = value["body"]["content"];
          }   
        },
        (error) => {  
        }) 
    }
     
  }

  getQuestions(){   
    this.questions = []
    this.questionService.findAllByExam(this.examId,0).subscribe(  
      (value) => {
        this.questions= value["body"]["content"] 
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
/******************Pagination *****************/
setPage(page: number) {
  if ( page < this.totalPages  && 0 <= page   ){   
    if (this.queryField['value']){
    }
    else{
      this.currentPage = page
      this.questionService.findAllByExam(this.examId,page).subscribe(  
        (value) => { 
          this.questions= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          //this.setPage(1);
        }
      );
    }
     
  }  
}
/******************Pagination *****************/
/****************** Sort *****************/
  sort_by_title(key:string){
    if (this.queryField['value']){
      
    }
    else
    {
      if (this.compTitle % 2 == 0){
        this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            console.log(value)
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compTitle = this.compTitle + 1
  }

  sort_by_description(key:string){
    if (this.queryField['value']){
       
    }
    else
    {
      if (this.compDescription % 2 == 0){
        this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            console.log(value)
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compDescription = this.compDescription + 1
  }
}
