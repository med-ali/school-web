import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { ExamListComponent  } from './exam-list.component';   
import { ExamDetailComponent } from './exam-detail.component';
import { QuestionListComponent  } from '../question-list/question-list.component';
import { AuthGuardService } from '../../dashboard/services/auth.guard.service';
import { UserGuardService } from '../../dashboard/services/user.guard.service';
export const ExamRoute: Routes = [
    { 
        path: 'exams',
        component: ExamListComponent,canActivate: [AuthGuardService,UserGuardService]
    },
    { 
        path: 'exams/:id/detail',
        component: ExamDetailComponent,canActivate: [AuthGuardService,UserGuardService]
    },
    { 
        path: 'exams/detail/questions/:id',
        component: QuestionListComponent,canActivate: [AuthGuardService,UserGuardService]
    } 
];

export const ExamPopupRoute: Routes = [
     
];