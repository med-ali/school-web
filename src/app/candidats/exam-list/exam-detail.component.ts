import { Component, OnDestroy, OnInit , ViewChild } from '@angular/core';
import { User } from '../../dashboard/models/User.model';
import { Exam } from '../../dashboard/models/Exam.model';
import { Subscription } from 'rxjs';
import { UserService } from '../../dashboard/services/user.service';
import { ExamService } from '../../dashboard/services/exam.service';
import { PagerService } from  '../../dashboard/services/pager.service'; 
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../dashboard/services/search.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http'; 
import { QuestionListComponent } from '../../dashboard//question-list/question-list.component'; 
@Component({
  selector: 'app-exam-detail',
  templateUrl: './exam-detail.component.html',
  styleUrls: ['./exam-detail.component.scss']
})
export class ExamDetailComponent implements OnInit {
  user: User
  userid : number
  examId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  exam: Exam
  examTitle : string
  examDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  usersExam : User[]
  queryField: FormControl = new FormControl();
  constructor(private modalService: NgbModal ,private examService: ExamService ,private userService: UserService,private router: Router,private route: ActivatedRoute) { }
   
  ngOnInit() {
    this.examService.findByid(this.route["snapshot"]["params"]["id"]).subscribe( (value) => { 
      this.exam = value["body"]["exam"] 
      this.examTitle = value["body"]["exam"]["title"]
      this.examDescription = value["body"]["exam"]["description"]
      this.examId = value["body"]["exam"]["id"]  
      this.usersExam = value["body"]["users"]["content"]  
    })
  }
  refresh(): void {
    window.location.reload();
  }
}
