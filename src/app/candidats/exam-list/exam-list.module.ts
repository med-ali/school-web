 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core'; 
import { ExamListComponent  } from './exam-list.component'; 
import { ExamDetailComponent  } from './exam-detail.component'; 
import {ExamRoute  } from './exam-list.route';
import {ExamPopupRoute } from './exam-list.route'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {MatDialogModule} from '@angular/material';  
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material'; 
const ENTITY_STATES = [
    ...ExamRoute,
    ...ExamPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        BrowserAnimationsModule,
        CommonModule,
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule,
        
    ],
    declarations: [
        ExamListComponent, 
        ExamDetailComponent 
    ],
    entryComponents: [ 
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamListModule { }
