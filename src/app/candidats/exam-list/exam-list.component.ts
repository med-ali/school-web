import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { User } from '../../dashboard/models/User.model';
import { Exam } from '../../dashboard/models/Exam.model';
import { Subscription } from 'rxjs';
import { UserService } from '../../dashboard/services/user.service';
import { ExamService } from '../../dashboard/services/exam.service';
import { PagerService } from '../../dashboard/services/pager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../dashboard/services/search.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { QuestionListComponent } from '../../candidats//question-list/question-list.component';
@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.scss']
})
export class ExamListComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  exams: Exam[]
  totalElements: number;
  totalPages: number;
  currentPage: number;
  pageView: number
  queryField: FormControl = new FormControl();
  compTitle: number = 0
  compDescription: number = 0
  //Iduser:number
  score: number
  constructor(private activatedRoute: ActivatedRoute, private _searchService: SearchService, private modalService: NgbModal, private examService: ExamService,
    private userService: UserService, private pagerService: PagerService, private route: ActivatedRoute, private router: Router,
  ) {
  }

  openViewQuestion(examId: number) {
    const modalRef = this.modalService.open(QuestionListComponent, { windowClass: 'fadeStyle', centered: true });
  }

  TabReturn(length) {
    return new Array(length);
  }

  ngOnInit() {
    this.currentPage = 0
    this.examService.findAllByUser(Number(localStorage.getItem('userId'))).subscribe(
      (value) => {
        this.exams = value["body"]["content"];
        this.totalElements = value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages > 6) {
          this.pageView = 6
        } else {
          this.pageView = this.totalPages
        }
      }
    );

  }

  getScore(userId: number, examId: number) {
    this.examService.findExamUser(Number(localStorage.getItem('userId')), examId).subscribe(
      (value) => {
        console.log(value["body"]["score"])
        if (value["body"]["score"] == null) {
          this.score = 0
        } else {
          this.score = value["body"]["score"]
        }
      }
    );
  }
}