 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {LoginRoute   } from './login.route'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material';
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { LoginComponent  } from './login.component'; 
const ENTITY_STATES = [
    ...LoginRoute
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule, 
    ],
    declarations: [
        LoginComponent
    ],
    entryComponents: [
    ],
    providers: [ 
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoginModule { }
