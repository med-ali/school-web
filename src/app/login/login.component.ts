import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { UserService } from '../dashboard/services/user.service';
import { LoginService } from '../dashboard/services/login.service';
import { Router } from '@angular/router';
import { User } from '../dashboard/models/User.model';
import {Subject} from 'rxjs';  
import { Message } from '../dashboard/models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from '../shared/alert/alert.component';
import { MessageService } from '../dashboard/services/message.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { NavbarService } from '../dashboard/services/navbar.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrlAdmin:string = "dashboard/users"
  returnUrluser:string = "dashboard/profil/"
  decodeToken:any;
  userRole:string;
  userId:any;
  user:User;
  private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
  constructor(public JwtHelperService: JwtHelperService,public nav: NavbarService,
    private modalService: NgbModal,private activeModal: NgbActiveModal,
    private MessageService: MessageService,private formBuilder: FormBuilder,
    private userService: UserService ,private loginService: LoginService,
    private eventManager: JhiEventManager,
    private router: Router  ) { 
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    } 
    
    ngOnInit() {
      this.initForm();
      if (localStorage.getItem('accessToken')) {
        console.log(localStorage.getItem('userRole'))
        this.loginService.LoggedIn()
        if (localStorage.getItem('userRole') == "ROLE_ADMIN"){
          
          this.router.navigate([this.returnUrlAdmin]);
        }else{
          this.router.navigate([this.returnUrluser+localStorage.getItem('userId')]);
        }
      } 
      else{
        this.loginService.logout()
      }
    }
    
    initForm() {
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required] 
      });
    }
    openErrorEmail() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: "Email is already in use!", status: false };
    }
    openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to login !', status: false };
    }
    onSubmitForm() {
      const formValue = this.loginForm.value; 
      this.loginService.login(formValue['email'],formValue['password']).subscribe(
        (value) => {
          console.log("value from sarray :" + value)
          console.log("value from sarray test2 :" + value)
          if (value["body"]!= null){
            console.log(this.JwtHelperService.decodeToken(value["body"]["jwtResponse"]["accessToken"]))          
            this.decodeToken = this.JwtHelperService.decodeToken(value["body"]["jwtResponse"]["accessToken"])
            this.userRole = this.decodeToken["roles"][0]["authority"]
            //this.decodeToken["userId"] =
            this.userId =  value["body"]["user"]["id"]
            this.user = this.decodeToken["user"]
            this.loginService.loggedIn.next(true);
            localStorage.setItem('accessToken', JSON.stringify(value["body"]["jwtResponse"]["accessToken"]));
            localStorage.setItem('userRole', value["body"]["user"]["roles"][0]["name"]);
            localStorage.setItem('firstName', value["body"]["user"]["firstName"]);
            localStorage.setItem('lastName', value["body"]["user"]["lastName"]);
            localStorage.setItem('userId', value["body"]["user"]["id"]);
            localStorage.setItem('currentUser', JSON.stringify(value["body"]["user"]));
            this.loginService.firstName = value["body"]["user"]["firstName"]
            this.loginService.lastName = value["body"]["user"]["lastName"]
            this.onLoginSuccess(value["body"]["user"]["roles"][0]["name"])
            if(value["body"]["user"]["roles"][0]["name"]=="ROLE_ADMIN"){
              console.log("loginadmin"+value["body"]["user"]["id"])
              
              this.router.navigate([this.returnUrlAdmin]);
              this.loginService.isAdmin.next(true)
            }else{
              //this.router.navigate([this.returnUrluser]);
              console.log("loginuser"+value["body"]["user"]["id"])
              this.router.navigateByUrl(this.returnUrluser+value["body"]["user"]["id"])
              //this.router.navigate([this.returnUrluser,value["body"]["user"]["id"]]);
              //window.location.href = this.returnUrluser+value["body"]["user"]["id"];
              this.loginService.isAdmin.next(false)
            }
          }
          else{
            this.openError();
          }
        },
        (error) => {
          this.openError();
        }) 
    } 

    private onLoginSuccess(user:any) {
      console.log(this.userId+"onLoginSuccessonLoginSuccessùùù"+user)
      this.eventManager.broadcast({ name: 'loginSuccess', content: {role:user,id:this.userId}});
      
    }

    logout() {
      localStorage.clear
      localStorage.removeItem('currentUser');
      localStorage.removeItem('accessToken');
      localStorage.removeItem('userId');
      localStorage.removeItem('userRole');
      this.currentUserSubject.next(null);
    } 
}
