import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { LoginComponent } from "./login.component";
import { NavbarService } from '../dashboard/services/navbar.service';

@Injectable()
export class NavigateAwayFromLoginDeactivatorService implements CanDeactivate<LoginComponent> {

  constructor(public nav: NavbarService) {  }

  canDeactivate(target: LoginComponent) {
    this.nav.show();
    return true;
  }
}