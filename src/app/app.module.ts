import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalDialogModule } from 'ngx-modal-dialog'; 
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { DashboardModule } from './dashboard/dashboard.module';
import { LoginModule } from './login/login.module';
import { CandidatsModule } from './candidats/candidats.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from './dashboard/services/user.service';
import { UserPopupService } from './dashboard/services/user-popup.service';
import { PagerService } from './dashboard/services/pager.service';
import { QuestionService } from './dashboard/services/question.service';
import { SearchService } from './dashboard/services/search.service'; 
import { AnswerService } from './dashboard/services/answer.service';
import { IdeaService } from './dashboard/services/idea.service'; 
import { QuoteService } from './dashboard/services/quote.service';
import { ExamService } from './dashboard/services/exam.service'; 
import { LoginService } from './dashboard/services/login.service'; 
import { MessageService } from './dashboard/services/message.service';
import { RoleService } from './dashboard/services/role.service';
import { SubjectService } from './dashboard/services/subject.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule} from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { MaterialModule } from './shared/shared.module';
import { PropositionService } from './dashboard/services/proposition.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { NavbarService } from './dashboard/services/navbar.service';
import { NavigateAwayFromLoginDeactivatorService } from './login/NavigateAwayFromLoginDeactivatorService';
import { AuthGuardService } from './dashboard/services/auth.guard.service';
import { AdminGuardService } from './dashboard/services/admin.guard.service';
import { UserGuardService } from './dashboard/services/user.guard.service';
import { JwtModule } from '@auth0/angular-jwt';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './dashboard/services/token.interceptor.service';
import { ClassService } from './dashboard/services/class.service';
import { NoteService } from './dashboard/services/etudiantSubjectNote.service';
import { EmailService } from './dashboard/services/email.service';
import { ChartsModule } from 'ng2-charts';
import { SearchPipe } from './dashboard/services/search.pipe';
import { DatePipe } from '@angular/common';
import { MatMaterialModule } from './shared/angular-material.module';
import { MatSelectModule,MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule } from '@angular/material';
export function tokenGetter() {
  return localStorage.getItem('access_token');
}
@NgModule({  
  declarations: [
    AppComponent
  ],
  entryComponents: [
    
  ],
  imports: [
    MatMaterialModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatMaterialModule,
    ChartsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,   
    ModalDialogModule.forRoot(),
    BrowserModule,
    NgbModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['http://localhost:8090/api'],
        blacklistedRoutes: ['example.com/examplebadroute/']
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DashboardModule,
    LoginModule,
    CandidatsModule,
    CommonModule,
    MaterialModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (http: HttpClient) => new TranslateHttpLoader(http, '/my-school/assets/i18n/', '.json'),
          //useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
      
  })
  ],
  providers: [ 
    UserService,
    SearchService,
    PagerService,
    UserPopupService,
    NgbActiveModal,
    ExamService,
    QuestionService,
    PropositionService,
    LoginService,
    AnswerService,
    MessageService,
    RoleService,
    NavbarService,
    NavigateAwayFromLoginDeactivatorService,
    AuthGuardService,
    AdminGuardService,
    UserGuardService,
    SubjectService,
    NoteService,
    EmailService,
    IdeaService,
    QuoteService,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    ClassService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
  
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/my-school/assets/i18n/', '.json');
}