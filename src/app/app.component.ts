import { Component, OnInit } from '@angular/core'; 
import { Observable, Subject,Subscription } from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import { NavbarService } from './dashboard/services/navbar.service';
import { LoginService } from './dashboard/services/login.service';
import { NavigationEnd, Router ,ActivatedRoute} from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { User } from './dashboard/models/User.model';
import 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'my-school';
  secondes = ""
  isLoggedIn$: Observable<boolean>;
  isAdmin$: Observable<boolean>;
  isLoggedIn:boolean;
  isAdmin:boolean;
  firstName:string;
  lastName:string;
  userId:any;
  close:boolean;
  eventSubscriber: Subscription;
  constructor(private loginService: LoginService,public nav: NavbarService,
    private router: Router,private translate: TranslateService,
    private breakpointObserver: BreakpointObserver,
    private eventManager: JhiEventManager, ) {
    translate.setDefaultLang('fr');
  }


  ngOnInit() {
    this.close = false;
    this.userId=localStorage.getItem('userId');
    this.registerChangeInLogin()
    this.lastName = localStorage.getItem('lastName')
    this.firstName = localStorage.getItem('firstName')
    this.loginService.firstName = localStorage.getItem('firstName')
    this.loginService.lastName = localStorage.getItem('lastName')
    this.isLoggedIn$ = this.loginService.isLoggedIn;
    this.isAdmin$ = this.loginService.getIsAdmin
    if (localStorage.getItem('accessToken')) {
      console.log(localStorage.getItem('userRole'))
      this.loginService.LoggedIn()
      this.close = false;
      if (localStorage.getItem('userRole') == "ROLE_ADMIN"){
        this.isAdmin = true
      }else{
        this.isAdmin = false
      }
    } 
    else{
      this.loginService.logout()
    }
    this.isLoggedIn$.subscribe(
      (value) => {
        if (value) {
          this.isLoggedIn = value
        }else{
          this.router.navigate(['/login']);
        }
      }
    )
    this.isAdmin$.subscribe(
      (value) => {
        console.log(value)
        if (value) {
          this.isAdmin = true    
        }
        else
        {
          this.isAdmin = false
        }
      }
    )
    if (localStorage.getItem('userRole') == "ROLE_ADMIN"){
      this.isAdmin = true
      this.loginService.isAdmin.next(true)
    }else{
      //this.toProfile()
      this.isAdmin = false
      this.loginService.isAdmin.next(false)
    }

  }

  useLanguage(language: string) {
    this.translate.use(language);
  }

  toProfile (){
    console.log("toprofile")
    this.router.navigate(['dashboard/profil/'+localStorage.getItem('userId')]);
  }

  registerChangeInLogin() {
    this.eventSubscriber = this.eventManager.subscribe('loginSuccess', (response) => this.changeAuth(response));
  }

  public changeAuth(response:any){
    console.log(localStorage.getItem('userId')+response.id+"llllllllllllllll"+response.role)
    this.userId = response.id = localStorage.getItem('userId');
    if (response.role == "ROLE_ADMIN"){
      this.isAdmin = true;
    }else{
      this.isAdmin = false;
      //this.toProfile()
      console.log("loginSuccessloginifSuccess"+response+this.isAdmin)
    }
    console.log("loginSuccessloginSuccess"+response+this.isAdmin)
  }

  logout() {
    //this.sidenav.toggle()
    this.close = true;
    this.loginService.logout()
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
  }
}
