import { Routes ,Route} from '@angular/router';
import { RouterModule } from '@angular/router';  
import { UserRoute ,UserPopupRoute } from './user-list/user-list.route';
import { ExamRoute ,ExamPopupRoute } from './exam-list/exam-list.route';
import { ClassRoute } from './class/class.route';
import { QuestionRoute ,QuestionPopupRoute } from './question-list/question-list.route';
import {UserAnswerRoute  } from './user-answer/user-answer.route';
import {UserAnswerPopupRoute } from './user-answer/user-answer.route';
import { DashboardComponent } from './dashboard.component';
import { AuthGuardService } from './services/auth.guard.service';
import { SubjectRoute } from './subject/subject.route';
import { RankRoute } from './rank/rank.route';
import { IdeaRoute } from './idea/idea.route';
import { QuoteRoute } from './quote/quote.route';
export const MODULE_ROUTES: Route[] = [
    {
      path:'dashboard', component: DashboardComponent, children: [
        ...UserPopupRoute,  
        ...UserRoute,
        ...ExamRoute,
        ...ExamPopupRoute,
        ...QuestionRoute,
        ...QuestionPopupRoute,
        ...UserAnswerRoute,
        ...UserAnswerPopupRoute,
        ...ClassRoute,
        ...SubjectRoute,
        ...RankRoute,
        ...IdeaRoute,
        ...QuoteRoute,
        { path: '', component: DashboardComponent, canActivate: [AuthGuardService] }
      ]
    }
];