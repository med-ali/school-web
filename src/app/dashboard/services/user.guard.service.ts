import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { User } from '../models/User.model';
import { Answer } from '../models/Answer.model';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { LoginService } from './login.service';

@Injectable()
export class UserGuardService implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.loginService.getIsAdmin.pipe(
      take(1),
      map((getIsAdmin: boolean) => {
        if (getIsAdmin) {
          this.loginService.logout()
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      })
    );
  }
}