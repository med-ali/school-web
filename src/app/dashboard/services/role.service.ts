import { Role } from '../models/Role.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { RoleUser } from '../models/RoleUser.model';import { SERVER_API_URL } from '../../app.constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<Role>;
@Injectable()
export class RoleService {
  UrlBase:string = SERVER_API_URL+'api';
  
  /******************* *******************/
  getAllRoles(): Observable<HttpResponse<Role[]>> {
    return this.httpClient.get<Role[]>(this.UrlBase+'/roles', { observe: 'response' })     
  } 
  saveRole(roleUser: RoleUser): Observable<EntityResponseType> {
    return this.httpClient.post<Role>(this.UrlBase+'/roles/users/create',roleUser, { observe: 'response' });
  }

  constructor(private httpClient: HttpClient,private router: Router) {  
  }
}
