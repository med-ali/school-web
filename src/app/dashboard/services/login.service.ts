import { User } from '../models/User.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { SERVER_API_URL } from '../../app.constants';
import { BehaviorSubject } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<User>;
@Injectable()
export class LoginService {
  UrlBase:string = SERVER_API_URL+'api';
  
  /******************* *******************/
  login(email: string,password:string): Observable<EntityResponseType> {
    return this.httpClient.post<User>(this.UrlBase+'/auth/signin', {"email":email,"password":password}, { observe: 'response' });
  }

  loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isAdmin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public firstName:string;
  public lastName:string;
  get  isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  LoggedIn(){
    this.loggedIn.next(true);
  }
  LoggedOut(){
    this.loggedIn.next(false);
  }
  checkRole(){
    this.isAdmin.next(true);
  }

  get  getIsAdmin() {
    return this.isAdmin.asObservable();
  }
  
  logout(){
    localStorage.clear
    localStorage.removeItem('firstName');
    localStorage.removeItem('lastName');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('userId');
    localStorage.removeItem('Iduser');
    localStorage.removeItem('userRole');  
    this.loggedIn.next(false);
    this.LoggedOut()
    console.log('rrrrrrrrrrrrrr')
  }

  constructor(private httpClient: HttpClient,private router: Router) { 
    
  }
}
