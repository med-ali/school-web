import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Clas } from '../models/class.model';
import { Quote } from '../models/Quote.model';
import { createRequestOption } from '../../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
export type EntityResponseType = HttpResponse<Clas>;
@Injectable()
export class QuoteService {

    private resourceUrl = SERVER_API_URL + 'api/quote';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/quote';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(quote: Quote): Observable<Quote> {
        //this.convertDate(article);
        return this.http.post<Quote>(this.resourceUrl, quote);
    }

    update(quote: Quote): Observable<Quote> {
        //this.convertDate(article);
        return this.http.put<Quote>(this.resourceUrl, quote);
    }

    find(id: number): Observable<Quote> {
        return this.http.get<Quote>(`${this.resourceUrl}/${id}`);
    }

    query(page:number,req?: any): Observable<HttpResponse<Quote[]>> {
        const options = createRequestOption(req);
        return this.http.get<Quote[]>(this.resourceUrl+'/page?page='+page, { params: options, observe: 'response' });
    }
    
    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
     
   
    search(req?: any): Observable<HttpResponse<Quote[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Quote[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.qty) {
                Params = Params.append('qty', req.qty)
            }

        }
        return Params;
    };
   
    updateVote(quote: Quote,idUser:any): Observable<Quote> {
        //this.convertDate(article);
        return this.http.put<Quote>(this.resourceUrl+"/vote/"+idUser, quote);
    }

    findQuoteByUser(userId:any,req?: any): Observable<HttpResponse<Quote[]>> {
        const options = createRequestOption(req);
        return this.http.get<Quote[]>(this.resourceUrl+'/user/'+userId, { params: options, observe: 'response' });
    }

    findQuoteByDate(date:any,page:number): Observable<HttpResponse<any[]>>{
        return this.http.get<Quote[]>(this.resourceUrl+'/date/?date='+date+'&page='+page , {  observe: 'response' })
    }
}
