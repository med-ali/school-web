import { HttpParams } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';

export const createRequestOption = (req?: any): HttpParams => {
    
    let Params = new HttpParams();
    if (req) {
        // Begin assigning parameters
        if(req.TabAnswer){
            Params = Params.append('TabAnswer', req.TabAnswer)
        }
    } 
    return Params;
};
