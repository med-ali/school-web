import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { createRequestOption } from '../services/request-util';
import { ExamUser } from '../models/ExamUser.model';
import { Proposition } from '../models/Proposition.model';
import { SERVER_API_URL } from '../../app.constants';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<Question>;
@Injectable()
export class QuestionService {
  
  /******************* *******************/
  UrlBase:string = SERVER_API_URL+'api';

  constructor(private httpClient: HttpClient,private router: Router) { 
  }

  createQcm(examId:number,question: Question,propositions:Proposition[]): Observable<EntityResponseType> {
    console.log(propositions)
    return this.httpClient.post<Question>(this.UrlBase+'/questions/qcm/'+examId+'/create', {question:question,propositions:propositions},{ observe: 'response' });
  }
  
  createQro(examId:number,question: Question ): Observable<EntityResponseType> {
    return this.httpClient.post<Question>(this.UrlBase+'/questions/qro/'+examId+'/create',  question  ,{ observe: 'response' });
  }
  findAllByExam(examId:number,page:number): Observable<HttpResponse<Question[]>> {
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/exam/'+examId+'/page?page='+page, { observe: 'response' })     
  }

  query(page:number): Observable<HttpResponse<Question[]>> {     
    return this.httpClient.get<Question[]>(this.UrlBase+'/exams/page?page='+page, {  observe: 'response' })
  }

  findByid(id: number): Observable<EntityResponseType> {
    return this.httpClient.get<Question>(this.UrlBase+'/questions/questionId/'+ id, { observe: 'response' })     
  }

  editQuestion(id:number,question: Question): Observable<EntityResponseType> {
    return this.httpClient.put<Question>(this.UrlBase+'/questions/'+id, question, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/questions/delete/'+ id, { observe: 'response' });
  }  
  
  findAllByTiyTitle(queryString: string,examId:number,page:number): Observable<HttpResponse<Question[]>> {
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/exam/'+examId+'/search/page/?title='+queryString+'&page='+page, { observe: 'response' })         
  }

  sortAsc(key:string, page:number,examId:number): Observable<HttpResponse<Question[]>> { 
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/exam/'+examId+'/sort/asc?key='+key+'&page='+page, {  observe: 'response' })
  }

  sortDesc(key:string, page:number,examId:number): Observable<HttpResponse<Question[]>> { 
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/exam/'+examId+'/sort/desc?key='+key+'&page='+page, {  observe: 'response' })
  }

  sortAscSearch(titre:String,key:string, page:number): Observable<HttpResponse<Question[]>> { 
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/sort/search/asc?title='+titre+'&key='+key+'&page='+page, {  observe: 'response' })
  }
  sortDescSearch(titre:String,key:string, page:number): Observable<HttpResponse<Question[]>> { 
    return this.httpClient.get<Question[]>(this.UrlBase+'/questions/sort/search/desc?title='+titre+'&key='+key+'&page='+page, {  observe: 'response' })
  }
}