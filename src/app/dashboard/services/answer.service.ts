import { User } from '../models/User.model';
import { Answer } from '../models/Answer.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<Answer>;
@Injectable()
export class AnswerService {
  UrlBase:string = SERVER_API_URL+'api';
   
  /******************* *******************/
  create(answer: Answer,userId:number,questionId:number): Observable<EntityResponseType> {
    return this.httpClient.post<Answer>(this.UrlBase+'/questions/'+questionId+'/users/'+userId+'/answers', answer, { observe: 'response' });
  }

  getAnswerByquestionAndUser(userId:number,questionId:number): Observable<HttpResponse<Answer[]>> {
    return this.httpClient.get<Answer[]>(this.UrlBase+'/questions/'+questionId+'/users/'+userId+'/answers',  { observe: 'response' });
  } 
  
  editAnswer(userId:number,answerId:number,answer: Answer): Observable<EntityResponseType> {
    return this.httpClient.put<Answer>(this.UrlBase+'/users/'+userId+'/answers/'+answerId, answer, { observe: 'response' });
  }

  deleteAnswer(id: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/answers/'+ id, { observe: 'response' });
  }

  constructor(private httpClient: HttpClient,private router: Router) { 
    
  }
}