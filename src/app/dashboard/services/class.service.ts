import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Clas } from '../models/class.model';
import { createRequestOption } from '../../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
export type EntityResponseType = HttpResponse<Clas>;
@Injectable()
export class ClassService {

    private resourceUrl = SERVER_API_URL + 'api/class';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/class';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(clas: Clas): Observable<Clas> {
        //this.convertDate(article);
        console.log(clas)
        return this.http.post<Clas>(this.resourceUrl, clas);
    }

    update(clas: Clas): Observable<Clas> {
        //this.convertDate(article);
        return this.http.put<Clas>(this.resourceUrl, clas);
    }

    find(id: number): Observable<Clas> {
        return this.http.get<Clas>(`${this.resourceUrl}/${id}`);
    }

    query(page:number,req?: any): Observable<HttpResponse<Clas[]>> {
        const options = createRequestOption(req);
        return this.http.get<Clas[]>(this.resourceUrl+'/page?page='+page, { params: options, observe: 'response' });
    }
    queryAssignedToProduct(id: number, req?: any): Observable<HttpResponse<Clas[]>> {
        const options = createRequestOption(req);
        return this.http.get<Clas[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }
    queryGetArticlesNotNormalised(req?: any): Observable<HttpResponse<Clas[]>> {
        const options = createRequestOption(req);
        return this.http.get<Clas[]>(`${this.resourceUrl}/notnormalised/`, { params: options, observe: 'response' });
    }
    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    deleteStudentClass(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(this.resourceUrl+"/student/"+id+"/delete", { observe: 'response' });
    }
    deleteProfClass(idE: number,idC: number): Observable<HttpResponse<any>> {
        return this.http.delete(this.resourceUrl+"/prof/"+idE+"/class/"+idC+"/delete", { observe: 'response' });
    }
   
    search(req?: any): Observable<HttpResponse<Clas[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Clas[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.qty) {
                Params = Params.append('qty', req.qty)
            }

        }
        return Params;
    };

    deleteNorm(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/norm/${id}`, { observe: 'response' });
    }
    checkCode(code: string): Observable<Clas> {
        return this.http.get<Clas>(this.resourceUrl+"/checkcode/?code="+code);
    }
    createClassStudent(idC:number,TabUserId:Number[]): Observable<HttpResponse<Clas[]>> {
        return this.http.post<Clas[]>(this.resourceUrl+'/'+idC+'/student/create',TabUserId  , {  observe: 'response' });
    }
    createClassProf(idC:number,TabUserId:Number[]): Observable<HttpResponse<Clas[]>> {
        return this.http.post<Clas[]>(this.resourceUrl+'/'+idC+'/prof/create',TabUserId  , {  observe: 'response' });
    }
    createClassSubject(idC:number,TabUserId:Number[]): Observable<HttpResponse<Clas[]>> {
        return this.http.post<Clas[]>(this.resourceUrl+'/'+idC+'/subject/create',TabUserId  , {  observe: 'response' });
    }
    deleteSubjectClass(idS: number,idC: number): Observable<HttpResponse<any>> {
        return this.http.delete(this.resourceUrl+"/subject/"+idS+"/class/"+idC+"/delete", { observe: 'response' });
    }
    findClassByProf(idProf:any,page:number,req?: any): Observable<HttpResponse<Clas[]>> {
        const options = createRequestOption(req);
        return this.http.get<Clas[]>(this.resourceUrl+'/prof/'+idProf+'/page?page='+page, { params: options, observe: 'response' });
    }
   
}
