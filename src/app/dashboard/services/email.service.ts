import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Clas } from '../models/class.model';
import { createRequestOption } from '../../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { Email } from '../models/Email.model';
export type EntityResponseType = HttpResponse<Clas>;
@Injectable()
export class EmailService {

    private resourceUrl = SERVER_API_URL + 'api/';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }
    sendEmail(emailUser:Email): Observable<any> {
        console.log(emailUser+"you")
        return this.http.post<any>(this.resourceUrl+'message/mail',emailUser);
    }
}
