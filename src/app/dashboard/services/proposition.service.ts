import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { createRequestOption } from './request-util';
import { ExamUser } from '../models/ExamUser.model';
import { Proposition } from '../models/Proposition.model';
import { SERVER_API_URL } from '../../app.constants';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<Proposition>;
@Injectable()
export class PropositionService {
  
  /******************* *******************/
  UrlBase:string = SERVER_API_URL+'api';

  constructor(private httpClient: HttpClient,private router: Router) { 
  }

  create(questionId:number,proposition : Proposition): Observable<EntityResponseType> {
    return this.httpClient.post<Proposition>(this.UrlBase+'/questions/'+questionId+'/propositions', proposition,{ observe: 'response' });
  } 
  
  update(questionId:number,propositionId:number,proposition : Proposition): Observable<EntityResponseType> { 
    return this.httpClient.put<Proposition>(this.UrlBase+'/questions/'+questionId+'/propositions/'+propositionId, proposition, { observe: 'response' });
  }

  getPropositionByQcmQuestion(questionId:number ,page:number): Observable<EntityResponseType> {
    return this.httpClient.get<Proposition>(this.UrlBase+'/questions/'+questionId+'/propositions/page?page='+page, { observe: 'response' });
  }  

  getAllPropositionByQcmQuestion(questionId:number ): Observable<EntityResponseType> {
    return this.httpClient.get<Proposition>(this.UrlBase+'/questions/'+questionId+'/propositions', { observe: 'response' });
  } 

  getAllPropositionByQcmQuestionByUser(userId:number,questionId:number ): Observable<EntityResponseType> {
    return this.httpClient.get<Proposition>(this.UrlBase+'/questions/'+questionId+'/users/'+userId+'/propositions', { observe: 'response' });
  }

  delete(questionId: number,propositionId: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/questions/'+questionId+'/propositions/'+propositionId, { observe: 'response' });
  }  
  
  saveUserAnswer(userId:number,questionId:number,propositions : Array<Proposition>): Observable<HttpResponse<Array<Proposition>>> { 
    return this.httpClient.post<Array<Proposition>>(this.UrlBase+'/questions/'+questionId+'/users/'+userId+'/propsitions', propositions, { observe: 'response' });
  }
}