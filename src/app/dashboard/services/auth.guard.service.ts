import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { User } from '../models/User.model';
import { Answer } from '../models/Answer.model';
import { SERVER_API_URL } from '../../app.constants';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { LoginService } from './login.service';
import decode from 'jwt-decode';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {}
  public getToken(): string {
    return localStorage.getItem('accessToken');
  }

  /*public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return tokenNotExpired(token);
  }*/
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.loginService.isLoggedIn.pipe(
      take(1),
      map((isLoggedIn: boolean) => {
        if (!isLoggedIn) {
          this.loginService.logout()
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      })
    );
  }
}