import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Subject } from '../models/subject.model';
import { createRequestOption } from '../../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
export type EntityResponseType = HttpResponse<Subject>;
@Injectable()
export class SubjectService {

    private resourceUrl = SERVER_API_URL + 'api/subject';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/subject';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(s: Subject): Observable<Subject> {
        return this.http.post<Subject>(this.resourceUrl,s);
    }

    update(s: Subject): Observable<Subject> {
        //this.convertDate(article);
        return this.http.put<Subject>(this.resourceUrl, s);
    }

    find(id: number): Observable<Subject> {
        return this.http.get<Subject>(`${this.resourceUrl}/${id}`);
    }

    query(page:number,req?: any): Observable<HttpResponse<Subject[]>> {
        const options = createRequestOption(req);
        return this.http.get<Subject[]>(this.resourceUrl+'/page/?page='+page, { params: options, observe: 'response' });
    }
     
    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
     
    search(req?: any): Observable<HttpResponse<Subject[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Subject[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
    deleteSubjectProf(idS: number,idP: number): Observable<HttpResponse<any>> {
        return this.http.delete(this.resourceUrl+"/"+idS+"/prof/"+idP+"/delete", { observe: 'response' });
    }
    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.qty) {
                Params = Params.append('qty', req.qty)
            }

        }
        return Params;
    };

    /*findNormByArticle(id: number): Observable<ArticleNormalisation> {
        return this.http.get(`${this.resourceUrl}/${id}/norm`)
    }

    createNorm(articleNormalisation: ArticleNormalisation): Observable<ArticleNormalisation> {
        return this.http.post<ArticleNormalisation>(`${this.resourceUrl}/norm`, articleNormalisation);
    }

    createNormInventory(articleNormalisation: ArticleNormalisation,idShelf:number,qty: number): Observable<ArticleNormalisation> {
        console.log(articleNormalisation)
        return this.http.post<ArticleNormalisation>(`${this.resourceUrl}/norm/inventory/`+idShelf+"/?qty="+qty, articleNormalisation);
    }
    
    updateNorm(articleNormalisation: ArticleNormalisation): Observable<ArticleNormalisation> {
        return this.http.put<ArticleNormalisation>(`${this.resourceUrl}/norm`, articleNormalisation);
    }*/
    getAllSubjectsNoInscritByClass(id:number,page:number): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/notassociated/class/'+id+'/page/?page='+page, {  observe: 'response' })
    }
    getSubjectsSearchByLastNameNoInscritByClass(id:number,page:number,s:string): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/search/notassociated/class/'+id+'/page/?page='+page+'&q='+s, {  observe: 'response' })
    }
    findSubjectsByClassId(id:number,page:number): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/class/'+id+'/page/?page='+page, {  observe: 'response' })
    }
    findAllSubjectsByClassId(id:number): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/class/'+id, {  observe: 'response' })
    }
    createProfSubject(idC:number,TabUserId:Number[]): Observable<HttpResponse<Subject[]>> {
        return this.http.post<Subject[]>(this.resourceUrl+'/'+idC+'/prof/create',TabUserId  , {  observe: 'response' });
    }
    '/subject/etudiants/{idE}/page'
    findSubjectsByEudiantId(id:any,page:number): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/etudiants/'+id+'/page/?page='+page, {  observe: 'response' })
    }
    findAllSubjectsByEudiantId(id:any): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/etudiants/'+id, {  observe: 'response' })
    }
    "/subject/{idS}/note/etudiants/{idE}/page"
    findSubjectNoteByEudiantId(idE:number,idS:number,page:number): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+"/"+idE+'/note/etudiants/'+idS+'/page/?page='+page, {  observe: 'response' })
    }
    findSubjectNoteByEudiantIdByDate(idE:number,idS:number,page:number,date:any): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+"/"+idE+'/note/date/etudiants/'+idS+'/page/?page='+page +'&date='+date, {  observe: 'response' })
    }
    findSubjectNoteByEudiantIdByMonth(idE:number,idS:number,page:number,date:any): Observable<HttpResponse<Subject[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+"/"+idE+'/note/month/etudiants/'+idS+'/page/?page='+page +'&date='+date, {  observe: 'response' })
    }
    findSubjectRankByMonth(idC:number,idS:number,date:any,page:number): Observable<HttpResponse<any[]>>{
        return this.http.get<Subject[]>(this.resourceUrl+'/'+idC+'/rank/month/etudiants/'+idS+'/date/?date='+date+'&page='+page , {  observe: 'response' })
    }
    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        // ensure current page isn't out of range
        if (currentPage < 1) { 
            currentPage = 1; 
        } else if (currentPage > totalPages) { 
            currentPage = totalPages; 
        }
        
        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
}
