import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthGuardService } from './auth.guard.service';
import { Observable } from 'rxjs';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public auth: AuthGuardService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.getToken()){
      request = request.clone({ headers: request.headers.append('Authorization', 'Bearer ' + this.auth.getToken().replace('"',"").replace('"',"")) });
    }
    return next.handle(request);
  }
}