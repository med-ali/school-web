import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { ExamUser } from '../models/ExamUser.model';
import { SERVER_API_URL } from '../../app.constants';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<Exam>;
@Injectable()
export class ExamService {
  
  /******************* *******************/
  UrlBase:string = SERVER_API_URL+'api';

  createExamUser(TabUserExam:ExamUser[]): Observable<HttpResponse<Exam[]>> {
    return this.httpClient.post<Exam[]>(this.UrlBase+'/exams/users/create',TabUserExam  , {  observe: 'response' });
  }

  editExam(id:number,exam: Exam): Observable<EntityResponseType> {
    return this.httpClient.put<Exam>(this.UrlBase+'/exams/'+id, exam, { observe: 'response' });
  }

  create(exam: Exam): Observable<EntityResponseType> {
    return this.httpClient.post<Exam>(this.UrlBase+'/exams/create', exam, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/exams/'+ id, { observe: 'response' });
  }

  findByid(id: number): Observable<EntityResponseType> {
    return this.httpClient.get<Exam>(this.UrlBase+'/exams/examId/'+ id, { observe: 'response' })     
  }

  findAll(): Observable<HttpResponse<Exam[]>> {
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams', { observe: 'response' })     
  }
  
  query( page:number): Observable<HttpResponse<Exam[]>> {     
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/page?page='+page, {  observe: 'response' })
  }

  findAllByTiyTitle(queryString: string,page:number): Observable<HttpResponse<Exam[]>> {
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/search/?title='+queryString+'&page='+page, { observe: 'response' })     
  } 
  findAllByUser(userId:number): Observable<HttpResponse<Exam[]>> {
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/'+userId+'/users', { observe: 'response' })     
  }
  findExamUser(userId:number,examId:number): Observable<HttpResponse<ExamUser>> {
    return this.httpClient.get<ExamUser>(this.UrlBase+'/exams/'+examId+'/users/'+userId, { observe: 'response' })     
  }
  querySearch(queryString: string,page:number): Observable<HttpResponse<Exam[]>> {
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/search/?title='+queryString+'&page='+page, { observe: 'response' })     
  }

  deleteUserExamRelation(userId:number,examId:number ): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/exams/'+examId+'/users/'+userId, { observe: 'response' })     
  }

  getNoteUserByExam(userId:number,examId:number ): Observable<HttpResponse<any>> { 
    return this.httpClient.delete<any>(this.UrlBase+'/note/users/'+userId+'/exams/'+examId, { observe: 'response' })     
  }

  sortAsc(key:string, page:number): Observable<HttpResponse<Exam[]>> { 
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/sort/asc?key='+key+'&page='+page, {  observe: 'response' })
  }
  sortDesc(key:string, page:number): Observable<HttpResponse<Exam[]>> { 
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/sort/desc?key='+key+'&page='+page, {  observe: 'response' })
  }
  sortAscSearch(titre:String,key:string, page:number): Observable<HttpResponse<Exam[]>> { 
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/sort/search/asc?title='+titre+'&key='+key+'&page='+page, {  observe: 'response' })
  }
  sortDescSearch(titre:String,key:string, page:number): Observable<HttpResponse<Exam[]>> { 
    return this.httpClient.get<Exam[]>(this.UrlBase+'/exams/sort/search/desc?title='+titre+'&key='+key+'&page='+page, {  observe: 'response' })
  }
  constructor(private httpClient: HttpClient,private router: Router) { 
    
  }
}