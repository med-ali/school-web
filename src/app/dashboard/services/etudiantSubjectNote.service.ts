import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Clas } from '../models/class.model';
import { createRequestOption } from '../../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
export type EntityResponseType = HttpResponse<Clas>;
@Injectable()
export class NoteService {

    private resourceUrl = SERVER_API_URL + 'api/note';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/note';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(note: EtudiantSubjectNote): Observable<Clas> {
        //this.convertDate(article);
        return this.http.post<EtudiantSubjectNote>(this.resourceUrl, note);
    }

    update(note: EtudiantSubjectNote): Observable<EtudiantSubjectNote> {
        //this.convertDate(article);
        return this.http.put<EtudiantSubjectNote>(this.resourceUrl,note);
    }

    find(id: number): Observable<EtudiantSubjectNote> {
        return this.http.get<EtudiantSubjectNote>(`${this.resourceUrl}/${id}`);
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
      
}
