import { Component, OnInit,ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { RankEtudiant } from '../models/RankEtudiant.model';
import { Clas } from '../models/class.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { FormControl } from '@angular/forms';
import { Idea } from '../models/idea.model';
import { Quote } from '../models/Quote.model';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { ClassService } from '../services/class.service';
import { IdeaService } from '../services/idea.service';
import { QuoteService } from '../services/quote.service';
import { NewQuoteComponent } from './new-quote.component';
import { DeleteQuoteComponent } from './delete-quote.component';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  user: User
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  queryField: FormControl = new FormControl();
  isAdmin:boolean;
  type:string;
  clas:string;
  notes:EtudiantSubjectNote[];
  eventSubscriber: Subscription;
  dateSelected:any;
  phone:string;
  address:string;
  classes:Clas[]
  dateSelectedMonth:any;
  moyByMonth:number;
  isMonth:boolean;
  rank:number;
  selectedClass:Clas;
  ranksEtudiants:RankEtudiant[];
   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];
   // array of all items to be paged
   private allItems: any[];
   quotes:Quote[]
   quotesByUser:Idea[]
   quotesByUserId:any[]
  constructor(private subjectService: SubjectService,
    private datePipe: DatePipe,
    private eventManager: JhiEventManager,
    private userService: UserService,private router: Router,
    private modalService: NgbModal,
    private classService: ClassService,
    private ideaService: IdeaService,
    private quoteService: QuoteService,
    private route: ActivatedRoute) {
    }
  ngOnInit() {
    this.findQuoteByUser(localStorage.getItem('userId'));
    this.isMonth = false;
    this.registerChangeInLocations()
    this.currentPage = 0
    if (localStorage.getItem('userRole')== "ROLE_ADMIN"){
      this.isAdmin = true
    }
    
    this.userService.findByid(localStorage.getItem('userId')).subscribe( (value) => {
       this.user =  value["body"];
       this.userLastName = value["body"]["lastName"]
       this.userFirstName = value["body"]["firstName"]
       this.userEmail = value["body"]["email"]
       this.type = value["body"]["type"]
       this.userRole = value["body"]["roles"][0]["name"]
       this.phone = value["body"]["phone"]
       this.address = value["body"]["address"]
       this.selectedClass = value["body"]["clas"]
       if (this.type == "etudiant" && value["body"]["clas"]!=null){
         this.clas = value["body"]["clas"]["name"]
         this.loadAll()
         
       }
       else{
        this.loadAll()
       }
    })
    console.log(this.userRole+ ''+this.isAdmin)
  }

  
  //pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
  setPageS(page: number) {
    // get pager object from service
    this.pager = this.subjectService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
}
  loadAll(){
    this.quoteService.query(0).subscribe( (value) => {
      this.quotes = value["body"]["content"];
      this.quotes.forEach(element => {
        element.createdAt = element.createdAt.split("/")[0]
      });
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"] 
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      } 
    })
  }

  openNew() {
    const modalRef = this.modalService.open( NewQuoteComponent, {windowClass:  'fadeStyle' , centered: true});
    // modalRef.componentInstance.id = id;
  }

  query(page:number){
    this.quoteService.query(page).subscribe( (value) => {
      this.quotes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
      })
  }
  findQuoteByUser(idUser:any){
    this.quoteService.findQuoteByUser(idUser).subscribe( (value) => {
      this.quotesByUser = value["body"]["content"];
      this.quotesByUserId = []
      this.quotesByUser.forEach(element => {
        this.quotesByUserId.push(element.id); 
      });
      })
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.isMonth){
        this.currentPage = page
        this.queryByMonth(page,this.dateSelectedMonth)
      }
      else{
        this.currentPage = page
         this.query(page)
      } 
    }  
  }
  previousState() {
    window.history.back();
  }
   
  onDateSelectedMonth(){
    this.dateSelectedMonth = this.datePipe.transform(this.dateSelectedMonth,"yyyy-MM-dd");
    this.isMonth = true;
    this.queryByMonth(0,this.dateSelectedMonth);
  }

  queryByMonth(page:number,date:any){
    this.quoteService.findQuoteByDate(date,page).subscribe( (value) => {
      this.quotes = value["body"]["content"];
      this.quotes.forEach(element => {
        element.createdAt = element.createdAt.split("/")[0]
      });
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"] 
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      } 
    })
  }

   toDate(date:any){
     return date.split('T')[0];
   }

  registerChangeInLocations() {
    this.eventSubscriber = this.eventManager.subscribe('QuoteListModification', (response) => {this.query(0)});
  }
  
  updateVote(idea:Idea){
    this.ideaService.updateVote(idea,this.user.id).subscribe( (value) => {
      idea = value;
      this.quotesByUserId.push(idea.id)
    })
  }
  openDelete(quote:any) {
    const modalRef = this.modalService.open( DeleteQuoteComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.quote = quote;
  }
}
