import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { QuoteService } from '../services/quote.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import { Quote } from '../models/Quote.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
import { JhiEventManager } from 'ng-jhipster';
@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.scss']
})
export class NewQuoteComponent implements OnInit {
  quoteForm: FormGroup;
  queryField: FormControl = new FormControl();
  TabDate : Array<any> = []
  users : Array<any> = []
  ExamDate:Date
  userschecked : Array<any> = []
  totalItems: any;
  queryCount: any;
  page: any;
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  isAnonymos:boolean;
  user: User;
  type:string;
  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private _searchService: SearchService ,private formBuilder: FormBuilder,
    private quoteService: QuoteService,private userService: UserService,
    private router: Router,private activeModal: NgbActiveModal,
    private eventManager: JhiEventManager) { }
    onChange() {
      this.isAnonymos =  !this.isAnonymos;
    }
  
    ngOnInit() {
      this.initForm();
      this.isAnonymos = false;
      this.userService.findByid(localStorage.getItem('userId')).subscribe( (value) => {
        this.user =  value["body"];
        if (this.type == "etudiant" && value["body"]["clas"]!=null){
        
        }
        else{
         
        }
     })
    }

    initForm() { 
      this.quoteForm = this.formBuilder.group({
        quote: ['', Validators.required]  
      });
    }
    
    refresh(): void {
      window.location.reload();
    }

    TabReturn(length){
      return new Array(length);
    }

    onSubmitForm() {
      const formValue = this.quoteForm.value;
      const newQuote = new Quote(
        new Date(),
        formValue['quote'],
        new Date(),
        this.user,
        null,
      );
      this.quoteService.create(newQuote).subscribe(
        (value) => { 
          this.MessageService.sendMessage("quote added")
          this.eventManager.broadcast({
            name: 'QuoteListModification',
            content: 'Add an quote'
        });
          this.clear()
        },
        (error) => { 
          this.clear()
          this.openError()
        })
    }

    openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
    }
  
  clear() {
    this.activeModal.dismiss('cancel');
  }
 
}
