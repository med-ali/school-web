
import { JhiPaginationUtil } from 'ng-jhipster';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
import { QuoteComponent } from './quote.component';
@Injectable()
export class ClassResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const QuoteRoute: Routes = [
    { 
        path: 'quote',
        component: QuoteComponent,canActivate: [AuthGuardService]
    }

];

export const idea: Routes = [
    {
        /*path: 'class-new',
        component: ClassPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.class.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'*/
    }
     
];
