import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { Quote } from '../models/Quote.model';
import { UserPopupService } from '../services/user-popup.service';
import { QuoteService } from '../services/quote.service';
import { MessageService } from '../services/message.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
@Component({
  selector: 'app-delete-quote',
  templateUrl: './delete-quote.component.html',
  styleUrls: ['./delete-quote.component.scss']
})
export class DeleteQuoteComponent implements OnInit {
    quoteId:number
    quote:Quote;
    user:User;
    constructor(
        private MessageService: MessageService,
        private quoteService: QuoteService,
        public activeModal: NgbActiveModal,
        private modalService: NgbModal
        ,private router: Router,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
    }
    openError() {
        const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
        modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    refresh(): void {
        window.location.reload();
    }
     
    confirmDelete() {
        this.quoteService.delete(this.quote.id).subscribe(
            (value) => {
                this.clear()
                this.MessageService.sendMessage("quote deleted")
                this.eventManager.broadcast({
                    name: 'QuoteListModification',
                    content: 'Delete an quote'
                });
            },
            (error) => {
                this.clear()
                this.MessageService.sendMessage("error")
                this.openError()
            }
        );  
    }
}
