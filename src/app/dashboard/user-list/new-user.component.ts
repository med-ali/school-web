import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import {Subject} from 'rxjs';  
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';
import { Role } from '../models/Role.model';
@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  userForm: FormGroup;
  rolesOption: Array<any>;
  constructor(private roleService: RoleService,private MessageService: MessageService,private formBuilder: FormBuilder,
    private userService: UserService,private modalService: NgbModal,
    private router: Router,private activeModal: NgbActiveModal) { } 
  openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to create !', status: false };
  }
  openErrorEmail() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: "Email is already in use!", status: false };
  }
  ngOnInit() {
    this.initForm();
    this.getAllRoles()
  }
  getAllRoles(){
    this.roleService.getAllRoles().subscribe(  
      (value) => {
         this.rolesOption = value["body"]["content"]
      }
    );  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      psw: ['', Validators.required],
      role: ['', Validators.required],
      type: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required]
    });
  }
 
  refresh(): void {
    window.location.reload();
  }

  onSubmitForm() {
    const formValue = this.userForm.value;
    const newUser = new User(
      null,
      formValue['firstName'],
      formValue['lastName'],
      formValue['email'],
      formValue['psw'],
      formValue['role'],
      formValue['type'],
      formValue['phone'],
      formValue['address']
    );

    this.userService.create(newUser).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("user added")
        if (value["body"]["message"]=="Email is already in use!"){
          this.openErrorEmail()
        }
      },
      (error) => {
        this.clear()
        this.openError()
    }) 
  }
} 