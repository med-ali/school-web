 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ProfilComponent } from './';
import { UserListComponent  } from './'; 
import {UserRoute  } from './user-list.route';
import {UserPopupRoute } from './user-list.route';
//import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DeleteUserComponent  } from './delete-user.component';
import {  EditUserComponent } from './edit-user.component';
import {MatDialogModule} from '@angular/material';
import {  NewUserComponent  } from './new-user.component';
import { SharedModule } from '../../shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { AddRoleComponent } from './add-role.component';
import { AddNoteComponent } from './add-note.component';
import { EditNoteComponent } from './edit-note.component';
import { AddUserToMailComponent } from './add-user-to-mail.component';
import { ChartsModule } from 'ng2-charts';
import { MatMaterialModule } from '../../shared/angular-material.module';
import { DatePipe } from '@angular/common';
const ENTITY_STATES = [
    ...UserRoute,
    ...UserPopupRoute,
];

@NgModule({
    imports: [
        MatMaterialModule,
        ChartsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        MatDialogModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
        
    ],
    declarations: [
        UserListComponent,
        ProfilComponent,
        DeleteUserComponent, 
        EditUserComponent,
        NewUserComponent,
        AddRoleComponent,
        AddNoteComponent,
        EditNoteComponent,
        AddUserToMailComponent
    ],
    entryComponents: [
        DeleteUserComponent, 
        EditUserComponent,
        NewUserComponent,
        AddRoleComponent,
        AddNoteComponent,
        EditNoteComponent,
        AddUserToMailComponent
    ],
    providers: [
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserListModule { }
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}