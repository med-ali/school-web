import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SearchService } from '../services/search.service';
import { User } from '../models/User.model';
import { FormControl } from '@angular/forms';
import { Clas } from '../models/class.model';
import { MMessage } from '../models/Message.model';
import { Email } from '../models/Email.model';
import { ClassUser } from '../models/ClassUser.model';
import { ClassService } from '../services/class.service';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { EmailService } from '../services/email.service';
@Component({
  selector: 'app-add-user-to-mail',
  templateUrl: './add-user-to-mail.component.html'
})
export class AddUserToMailComponent implements OnInit {

  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private router: Router,private activeModal: NgbActiveModal,
    private emailService: EmailService,
    private formBuilder: FormBuilder,private _searchService: SearchService,
    private userService: UserService) { }
  id:number
  classId:number
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  userClass:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  UsersChecked : Array<any> = []
  idUsersChecked : Array<any> = []
  TabDate : Array<any> = []
  sessionDate : any
  TabClassUser:ClassUser[]
  TabUserId:number[]
  class:Clas
  message:string
  userId:any
  currentUser:User
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to add candidat !', status: false };
  }

  TabReturn(length){
    return new Array(length);
  }

  onChange(user:any, isChecked: boolean) {
    if(isChecked) {
      this.UsersChecked.push(user);
      this.idUsersChecked.push(user.id);
      console.log(user.id+"this.0idUsers"+"Checked"+this.idUsersChecked) 
    } else {
      let index = this.UsersChecked.indexOf(user);
      this.UsersChecked.splice(this.UsersChecked.indexOf(user),1); 
      let idIndex = this.idUsersChecked.indexOf(user.id);
      this.idUsersChecked.splice(this.idUsersChecked.indexOf(user.id),1);
      console.log(user.id+"this.0idUsers"+"Checked"+this.idUsersChecked) 
    } 
  }

  isEmpty(Tab :any[]){
      let resultat:Boolean = false
      for (let e of Tab) {
        if (e===undefined) {
          resultat = true;
          break;
        }
      }
    return resultat;
  }

  onChangeDate(date:any){
    this.sessionDate = date 
  }
  getAll(){
    this.currentPage = 0
    this.userService.query(0).subscribe(  
      (value) => { 
        this.users= value["body"]["content"];
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }else{ 
          this.pageView =  this.totalPages
        } 
      }
    );  
  }
  submit(classId:number){
    this.TabClassUser = []
    this.TabUserId = []
    const newMesg = new MMessage(
      null, 
      this.message,
      this.currentUser,
      Date(),
      Date()   
    );
    
    this.UsersChecked.forEach(user => {
        const newClassUser = new ClassUser(
          user,
          this.class,
          this.sessionDate        
        );   
        this.TabClassUser.push(newClassUser)
        this.TabUserId.push(user.id)
        console.log(this.TabUserId+"lkl"+classId)
      });
      const email = new Email(
        newMesg,
        this.TabUserId 
      );
      console.log(this.message+'55555555555555555555')
      this.emailService.sendEmail(email).subscribe(
        (value) => {
          this.clear()  
          this.MessageService.sendMessage("email added")
        },
        (error) => {
          this.clear()
          this.openError()
      })  
  }
  
  findUserById(id:number){ 
    let user:User 
    for (let e of this.users) {
      if (e.id === id) {
        user = e ;
        break;
      }
    }
    return user;  
  }
  
  findUserExamById(id:number){ 
    let resultat:boolean=false 
    for (let e of this.TabClassUser) {
      if (e.users.id === id) {
        resultat = true
        break;
      }
    }
    return resultat;  
  }
  find(){
  this.userService.findByid(this.userId).subscribe( (value) => {
    this.currentUser =  value["body"]; 
 })
}
  ngOnInit() {
    this.userId=localStorage.getItem('userId')
    this.getAll();
    this.find()
    this.users= []
    this.currentPage = 0 
    this.TabClassUser = []
    this.sessionDate = "2019-07-30"
    
  }
  getAllProfNoinscrit(){
    this.userService.getAllProfsNoInscritByClass(this.classId,0).subscribe(  
      (value) => {
        this.users= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      } 
    );   
  }
  search(){ 
    if (this.queryField['value']){
      this.users = []
      this._searchService.findAllByName(this.queryField['value'],0).subscribe(
        (value) => { 
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.users = []
      this.userService.query(0).subscribe(  
        (value) => {
          this.users= value["body"]["content"]
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  setPage(page: number) {
    if ( page < this.totalPages && 0 <= page   ){   
      if (this.queryField['value']){
        this.users = []
        this.currentPage = page
        this._searchService.querySearch(this.queryField['value'],page).subscribe(  
          (value) => {
            this.users= value["body"]["content"] 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      else{
        this.currentPage = page
        this.users = []
        this.userService.query(page).subscribe(  
          (value) => {
            this.users= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
          }
        );
      }   
    }  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  refresh(): void {
    window.location.reload();
  }
}
