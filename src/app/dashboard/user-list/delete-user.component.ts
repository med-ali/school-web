import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { UserPopupService } from '../services/user-popup.service';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { NoteService } from '../services/etudiantSubjectNote.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
    userId:number;
    note:EtudiantSubjectNote;
    constructor(
        private noteService: NoteService,
        private MessageService: MessageService,
        private userService: UserService,
        public activeModal: NgbActiveModal,
        private modalService: NgbModal
        ,private router: Router,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
    }
    openError() {
        const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
        modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    refresh(): void {
        window.location.reload();
    }
     
    confirmDelete(id: number) {
        if (id){
            this.userService.delete(id).subscribe(
                (value) => {
                  this.clear()
                  this.MessageService.sendMessage("user deleted")
                },
                (error) => {
                    this.clear()
                    this.MessageService.sendMessage("error")
                    this.openError()
                }
            );
        }
        else{
            this.noteService.delete(this.note.id).subscribe(
                (value) => {
                  this.clear()
                  this.MessageService.sendMessage("note deleted")
                  this.eventManager.broadcast({
                    name: 'NoteListModification',
                    content: 'Deleted an note'
                });
                },
                (error) => {
                    this.clear()
                    this.MessageService.sendMessage("error")
                    this.openError()
                }
            );
        }
    }
}
