import { Component, OnDestroy, OnInit , ViewChild ,Input} from '@angular/core';
import { User } from '../models/User.model';
import { Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { PagerService } from  '../services/pager.service';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface'; 
import { Router,ActivatedRoute , NavigationEnd} from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {  EditUserComponent } from './edit-user.component';
import {  NewUserComponent } from './new-user.component';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http'; 
import { UserPopupService } from '../services/user-popup.service';
import { DeleteUserComponent  } from './delete-user.component'; 
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';
import { AddRoleComponent } from './add-role.component';
import { NavbarService } from '../services/navbar.service';
//import swal from 'sweetalert2';
//import { ANIMATION_TYPES } from 'ngx-loading';
import { TranslateService } from '@ngx-translate/core';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { AddUserToMailComponent } from './add-user-to-mail.component';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list-test.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  compFirstName:number = 1
  compLastName:number = 1
  compEmail:number = 1
  queryField: FormControl = new FormControl();
  eventSubscriber: Subscription;
  message: any;
  subscription: Subscription;
  sortEmail:boolean = true;
  sortLastName:boolean = true;
  sortFirstName:boolean = true;
  @ViewChild(AlertComponent) alert: AlertComponent; 
  @ViewChild(DeleteUserComponent) Delete: DeleteUserComponent; 
  constructor(public nav: NavbarService,private roleService: RoleService,private MessageService: MessageService,private eventManager: JhiEventManager,private translate: TranslateService , private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,private _searchService: SearchService,
    private modalService: NgbModal  ,private userService: UserService, private pagerService: PagerService,private route: ActivatedRoute,private router: Router,
     ) { 
      this.subscription = this.MessageService.getMessage().subscribe(message => { 
        if (message["text"] == "user added" || message["text"] == "user updated" || message["text"] == "user deleted")  
          {
            this.getAll()
            this.getAllRoles()
            this.message = message;
          }
        
       });
   }
  /****** Open Modal *******/
  open(id:number) {
    const modalRef = this.modalService.open(DeleteUserComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.userId = id;
  }
  

  openEdit(user:User) {
    const modalRef = this.modalService.open( EditUserComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = user;
  }

  addRole(user:User) {
    const modalRef = this.modalService.open(  AddRoleComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = user;
  }
   
  openNew() {
    const modalRef = this.modalService.open( NewUserComponent, {windowClass:  'fadeStyle' , centered: true});
  } 
  
  search(){ 
    if (this.queryField['value']){
      this._searchService.findAllByName(this.queryField['value'],0).subscribe(
        (value) => { 
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.userService.query(0).subscribe(  
        (value) => {
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }

  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }

  ngOnInit() {
    this.getAll();
    this.getAllRoles();
  }
  
  getAllRoles(){
    this.roleService.getAllRoles().subscribe(  
      (value) => { 
        console.log(value)
      }
    );  
  }
  
  getAll(){
    this.currentPage = 0
    this.userService.query(0).subscribe(  
      (value) => { 
        this.users= value["body"]["content"];
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }else{ 
          this.pageView =  this.totalPages
        } 
      }
    );  
  }
  openNewEmail(clas:any) {
    const modalRef = this.modalService.open( AddUserToMailComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.clas = clas;
}
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        this._searchService.querySearch(this.queryField['value'],page).subscribe(  
          (value) => {
            this.users= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }
      else{
        this.currentPage = page
        this.userService.query(page).subscribe(  
          (value) => { 
            this.users= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
  ngOnDestroy() {
     
  }
  
  success(){ 
    var msg: Message ={ message: 'deleted successfully!', status: true };
    this.alert.show(msg);
  }

  failed(){ 
    var msg: Message ={ message: 'Failed to delete!', status: false }
    this.alert.show(msg);
  }
 
  sort_by_firstName(key:string){
    if (this.queryField['value']){
      if (this.compFirstName % 2 ==0){
        this.sortFirstName = true
        this.userService.sortAscSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => {  
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortFirstName = false
        this.userService.sortDescSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => { 
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }else{
      if (this.compFirstName % 2 ==0){
        this.sortFirstName = true
        this.userService.sortAsc(key,this.currentPage).subscribe( 
          (value) => {
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortFirstName = false
        this.userService.sortDesc(key,this.currentPage).subscribe( 
          (value) => {  
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    } 
    this.compFirstName = this.compFirstName + 1
  } 

  sort_by_lastName(key:string){
    if (this.queryField['value']){
      if (this.compLastName % 2 ==0){
        this.sortLastName = true
        this.userService.sortAscSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => {
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else
      {
        this.sortLastName = false
        this.userService.sortDescSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => {
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    else
    {
    if (this.compLastName % 2 ==0){
      this.sortLastName = true
      this.userService.sortAsc(key,this.currentPage).subscribe( 
        (value) => { 
          this.users = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      );
    }
    else{
      this.sortLastName = false
      this.userService.sortDesc(key,this.currentPage).subscribe( 
        (value) => { 
          this.users = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      ); 
    }
  }
    this.compLastName = this.compLastName + 1
  }
  
  sort_by_email(key:string){
    if (this.queryField['value']){
      if (this.compEmail % 2 ==0){
        this.sortEmail = true
        this.userService.sortAscSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => {
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortEmail = false
        this.userService.sortDescSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => {
            this.users = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    else
    {
    if (this.compEmail % 2 ==0){
      this.sortEmail = true
      this.userService.sortAsc(key,this.currentPage).subscribe( 
        (value) => { 
          this.users = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      );
    }
    else{
      this.sortEmail = false
      this.userService.sortDesc(key,this.currentPage).subscribe( 
        (value) => { 
          this.users = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      ); 
    }
  }
    this.compEmail = this.compEmail + 1
  }
}

