import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { Role } from '../models/Role.model';
import { RoleUser } from '../models/RoleUser.model';
import { UserPopupService } from '../services/user-popup.service';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {
  roleForm: FormGroup;
  user:User
  rolesOption : Role[];
  constructor(private formBuilder: FormBuilder,
    private roleService: RoleService,private MessageService: MessageService,
    private userService: UserService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal
    ,private router: Router,
    private eventManager: JhiEventManager) { 
    
  }

  ngOnInit() {
    this.initForm();
    this.getAllRoles()
  }

  initForm() {
    this.roleForm = this.formBuilder.group({
      role: ['', Validators.required]
    });
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }

  getAllRoles(){
    this.roleService.getAllRoles().subscribe(  
      (value) => {
         this.rolesOption = value["body"]["content"]
      }
    );  
  }

  clear() {
      this.activeModal.dismiss('cancel');
  }

  onSubmitForm() {
    const formValue = this.roleForm.value;
    const newRole = new Role(    
      formValue['role'],
      null
    ); 
    
    const newRoleUser = new RoleUser(     
      this.user,
      newRole
    ); 
   console.log(newRoleUser) 
   this.roleService.saveRole(newRoleUser).subscribe(
      (value) => {
        console.log(value);
        this.clear()
        this.MessageService.sendMessage("role added")
      },
      (error) => {
        console.log('Erreur ! : ' + error);
          this.clear()
          this.MessageService.sendMessage("error")
          this.openError()
      }
    );
}
}
