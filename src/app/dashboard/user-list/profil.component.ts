import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { Clas } from '../models/class.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { FormControl } from '@angular/forms';
import { Subject } from '../models/subject.model';
import { DeleteUserComponent  } from './delete-user.component'; 
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AddNoteComponent } from './add-note.component';
import { EditNoteComponent } from './edit-note.component';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { ClassService } from '../services/class.service';
import { toDate } from '@angular/common/src/i18n/format_date';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  user: User
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  exams : Array<Exam>
  queryField: FormControl = new FormControl();
  isAdmin:boolean;
  type:string;
  subjects:Subject[];
  selectedSubject:Subject;
  clas:string;
  notes:EtudiantSubjectNote[];
  eventSubscriber: Subscription;
  dateSelected:any;
  phone:string;
  address:string;
  classes:Clas[]
  dateSelectedMonth:any;
  moyByMonth:number;
  isMonth:boolean;
  rank:number;
  clasEtudiantNumber:number;
  isNumber:boolean;
  barChartLabelsM:string[];
  MoyByMonth:number[];
  RankByMonth:any[];
  barChartDataM:any[];
  barChartDataR:any[];
  chart:boolean;
  barChartLabelsR:any[];
  //S chart
  barChartLabelsMS:string[];
  barChartDataMS:any[];
  barChartDataRS:any[];
  barChartLabelsRS:any[];
  chartS:boolean;
  MoyByMonthS:any[]
  RankByMonthS:any[];
  constructor(private subjectService: SubjectService,
    private eventManager: JhiEventManager,
    private userService: UserService,private router: Router,
    private modalService: NgbModal,
    private classService: ClassService,
    private datePipe: DatePipe,
    private route: ActivatedRoute) { }
    public barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true
    };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'}
  ];

  ngOnInit() {
    this.chart = false;
    this.chartS = false;
    this.barChartLabelsM = [];
    this.RankByMonth = [];
    this.MoyByMonth = [];
    this.barChartDataM = [];
    this.barChartDataR = [];
    this.isMonth = false;
    this.registerChangeInLocations()
    this.currentPage = 0
    if (localStorage.getItem('userRole')== "ROLE_ADMIN"){
      this.isAdmin = true
    }
    
    this.userService.findByid(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
       this.user =  value["body"];
       this.userLastName = value["body"]["lastName"]
       this.userFirstName = value["body"]["firstName"]
       this.userEmail = value["body"]["email"]
       this.type = value["body"]["type"]
       this.userRole = value["body"]["roles"][0]["name"]   
       this.exams = value["body"]["exams"]
       this.phone = value["body"]["phone"]
       this.address = value["body"]["address"]
       if (this.type == "etudiant" && value["body"]["clas"]!=null){
         this.clas = value["body"]["clas"]["name"]
        this.subjectService.findAllSubjectsByEudiantId(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
          if (value["body"]) {
            this.subjects = value["body"]["content"];
            /*value["body"]["content"].forEach(element => {
              this.barChartLabelsM.push(element.title);
              console.log(element+"element")
            });*/
          }
       })
       }
       else{
        this.classService.findClassByProf(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => {
          if (value["body"]) {
            this.classes = value["body"]["content"];
          }
       })
       }
    })
  }

  toDate(date:any){
     return date.split("T")[0];
  }
  toMoy(num:any){
    return num.toFixed(2)
  }
  getEtudiantNumber(clas:Clas){
    this.userService.getEtudiantNumber(clas.id).subscribe( (value) => {
      console.log(value)
      this.clasEtudiantNumber = value;
      this.isNumber = true;
        let idIndex = this.classes.indexOf(clas);
        clas.etudiantNumber = value;
     
   })
  }

  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }

  onDateSelected(){
    
    this.queryByDate(0,this.dateSelected);
  }

  onDateSelectedMonth(){
    console.log("dddddateee"+this.chart)
    this.chart = false;
    this.chartS = false;
    console.log("ddddd"+this.chart)
    this.dateSelectedMonth = this.datePipe.transform(this.dateSelectedMonth,"yyyy-MM-dd");
    this.barChartLabelsM = [];
    this.RankByMonth = [];
    this.MoyByMonth = [];
    this.barChartDataM = [];
    this.isMonth = false;
    this.isMonth = true;
    this.queryByMonth(0,this.dateSelectedMonth);
  }

  loadAll(){
    this.subjectService.findSubjectsByEudiantId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => {
      this.subjects = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
    })
  }

  query(page:number){
    this.subjectService.findSubjectNoteByEudiantId(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,page).subscribe( (value) => {
      this.notes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
    })
  }
 
  queryByDate(page:number,date:any){
    this.subjectService.findSubjectNoteByEudiantIdByDate(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,page,date).subscribe( (value) => {
      this.notes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
      })
  }
  queryByMonth(page:number,date:any){
    this.subjectService.findSubjectNoteByEudiantIdByMonth(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,page,date).subscribe( (value) => {
      this.notes = value["body"]["notes"]["content"];
      this.totalElements=value["body"]["notes"]["totalElements"]
      this.totalPages = value["body"]["notes"]["totalPages"]
      this.moyByMonth = value["body"]["moy"]
      this.rank = value["body"]["rank"]["rank"]
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      }
      })
  }

  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.isMonth){
        this.currentPage = page
        this.queryByMonth(page,this.dateSelectedMonth)
      }
      else{
        this.currentPage = page
         this.queryByDate(page,this.dateSelected)
      }   
    }  
  }

  previousState() {
    window.history.back();
  }

  onOptionsSelected(){
    this.chart = false;
    this.chartS = false;
    this.isMonth = false ;
    if (this.dateSelectedMonth){
      this.queryByMonth(0,this.dateSelectedMonth);
    }
  }

  openNew() {
    const modalRef = this.modalService.open( AddNoteComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.subject = this.selectedSubject;
    if(this.isAdmin==true)
    {
      modalRef.componentInstance.isFromProf = this.isAdmin;
    }
    else{
      modalRef.componentInstance.isFromProf = false;
    }
  }

  openEdit(note:any) {
    const modalRef = this.modalService.open( EditNoteComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.subject = this.selectedSubject;
    modalRef.componentInstance.note = note;
    if(this.isAdmin == true)
    {
      modalRef.componentInstance.isFromProf = this.isAdmin;
    }
    else{
      modalRef.componentInstance.isFromProf = false;
    }
  }
  
  openDelete(note:any) {
    const modalRef = this.modalService.open(DeleteUserComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.note = note;
  }

  registerChangeInLocations() {
    this.eventSubscriber = this.eventManager.subscribe('NoteListModification', (response) => this.onOptionsSelected());
  }
  
  openChart(){
    this.userService.getChartSubjectByStudent(this.route["snapshot"]["params"]["id"],this.dateSelectedMonth).subscribe(value => {
         console.log(value+"chart")
    })
  }
  openChartMonth(){
    this.barChartLabelsM = [];
    this.RankByMonth = [];
    this.MoyByMonth = [];
    this.barChartDataM = [];
    this.barChartDataR = [];
    this.barChartLabelsR = [];
    this.userService.getChartSubjectByStudentInSixMounth(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,this.dateSelectedMonth,0).subscribe(value => {  
      let objMoy = {};
      let objRank = {};
      value.forEach(element => {
        this.barChartLabelsM.push(element.month);
        this.barChartLabelsR.push(element.month);
        this.MoyByMonth.push(element.moy.replace(",","."))
        this.RankByMonth.push(element.rank["rank"])
        console.log(value+"cha0000rt"+element.rank["rank"])
        objMoy["data"] = this.MoyByMonth;
        objMoy["label"] = "Moy" 
        objRank["data"] = this.RankByMonth;
        objRank["label"] = "Range" 
      });
      this.barChartDataM.push(objMoy);
      this.barChartDataR.push(objRank);
      this.chart = true;
      console.log(value+"chart"+this.barChartDataM)
    })
  }
  openChartMonthByStudent(){
    this.barChartLabelsMS = [];
    this.RankByMonthS = [];
    this.MoyByMonthS = [];
    this.barChartDataMS = [];
    this.barChartDataRS = [];
    this.barChartLabelsRS = [];
    this.userService.getChartSubjectByStudent(this.route["snapshot"]["params"]["id"],this.dateSelectedMonth).subscribe(value => {  
      let objMoy = {};
      let objRank = {};
      value["result"].forEach(element => {
        this.barChartLabelsMS.push(element.subject);
        this.barChartLabelsRS.push(element.subject);
        this.MoyByMonthS.push(element.source.moy.replace(",","."))
        this.RankByMonthS.push(element.source.rank["rank"])
        console.log(value+"cha0000rt"+element.source.rank["rank"])
        objMoy["data"] = this.MoyByMonthS;
        objMoy["label"] = "Moy";
        objRank["data"] = this.RankByMonthS;
        objRank["label"] = "Range";
      });
      this.barChartDataMS.push(objMoy);
      this.barChartDataRS.push(objRank);
      this.chartS = true;
      console.log(value+"chartS"+this.barChartDataMS)
    })
  }
}
