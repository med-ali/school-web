import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserListComponent  } from './user-list.component'; 
import { ProfilComponent } from './profil.component';
import { DeleteUserComponent } from './delete-user.component';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
export const UserRoute: Routes = [
    { 
        path: 'users',
        component: UserListComponent , canActivate: [AuthGuardService,AdminGuardService]
    },
    {
        path: 'profil/:id',
        component:  ProfilComponent, canActivate: [AuthGuardService]
    }
];
export const UserPopupRoute: Routes = [
    {
        path: 'user/:id/delete',
        component: DeleteUserComponent, canActivate: [AuthGuardService,AdminGuardService],
        outlet: 'popup'
    }
];