import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { Subject } from '../models/subject.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { NoteService } from '../services/etudiantSubjectNote.service';
import { UserPopupService } from '../services/user-popup.service';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class EditNoteComponent implements OnInit {
  noteForm: FormGroup;
  user:User;
  subject:Subject;
  note:EtudiantSubjectNote;
  isFromProf:boolean;
  constructor(private formBuilder: FormBuilder,
    private noteService: NoteService,private MessageService: MessageService,
    private userService: UserService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal
    ,private router: Router,
    private eventManager: JhiEventManager) { 
    
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.noteForm = this.formBuilder.group({
      note: ['', Validators.required],
      score: ['', Validators.compose([Validators.required, Validators.max(20), Validators.min(0)])],
    });
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }

 

  clear() {
      this.activeModal.dismiss('cancel');
  }

  onSubmitForm() {
    const formValue = this.noteForm.value;
    const newNote = new EtudiantSubjectNote(
      this.note.id,  
      this.user,
      this.subject,
      formValue['note'],
      this.note.score,
      this.note.createdAt,
      this.note.updatedAt,
      this.isFromProf
    ); 
    console.log(newNote);
   this.noteService.update(newNote).subscribe(
      (value) => {
        console.log(value);
        this.clear()
        this.MessageService.sendMessage("note added")
      },
      (error) => {
        console.log('Erreur ! : ' + error);
          this.clear()
          this.MessageService.sendMessage("error")
          this.openError()
      }
    );
}
}
