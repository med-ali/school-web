import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { User } from '../models/User.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';
import { Role } from '../models/Role.model';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild(AlertComponent) alert: AlertComponent;
  userForm: FormGroup;
  user:User
  rolesOption: Array<any>;
  constructor(private roleService: RoleService,private MessageService: MessageService,private formBuilder: FormBuilder,
    private userService: UserService,private modalService: NgbModal,
    private router: Router,private route: ActivatedRoute,private activeModal: NgbActiveModal) {
      
     }
    openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to edit !', status: false };
    }
    ngOnInit() {
      this.initForm();
      this.getAllRoles()
    }
    getAllRoles(){
      this.roleService.getAllRoles().subscribe(  
        (value) => {
           this.rolesOption = value["body"]["content"]
        }
      );  
    }
    initForm() {
      this.userForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', Validators.required],
        address: ['', Validators.required]
      });
  }
  refresh(): void {
    window.location.reload();
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  onSubmitForm(id :number) {  
    const formValue = this.userForm.value;
    const editUser = new User(
      id,
      formValue['firstName'],
      formValue['lastName'],
      formValue['email'],
      this.user.password,
      this.user.role,
      this.user.type,
      formValue['phone'],
      formValue['address']
    );
     
    this.userService.UpdateUser(id,editUser).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("user updated")  
      },
      (error) => {
        this.clear()
        this.openError()
      })  
  }  
  success(){ 
    var msg: Message ={ message: 'Updated successfully!', status: true };
    this.alert.show(msg);
  }

  failed(){ 
    var msg: Message ={ message: 'Failed to update!', status: false }
    this.alert.show(msg);
  }
}
