import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { IdeaService } from '../services/idea.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import { Idea } from '../models/idea.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
import { JhiEventManager } from 'ng-jhipster';
@Component({
  selector: 'app-new-idea',
  templateUrl: './new-idea.component.html',
  styleUrls: ['./new-idea.component.scss']
})
export class NewIdeaComponent implements OnInit {
  ideaForm: FormGroup;
  queryField: FormControl = new FormControl();
  TabDate : Array<any> = []
  users : Array<any> = []
  ExamDate:Date
  userschecked : Array<any> = []
  totalItems: any;
  queryCount: any;
  page: any;
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  isAnonymos:boolean;
  user: User;
  type:string;
  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private _searchService: SearchService ,private formBuilder: FormBuilder,
    private ideaService: IdeaService,private userService: UserService,
    private router: Router,private activeModal: NgbActiveModal,
    private eventManager: JhiEventManager) { }
    onChange() {
      this.isAnonymos =  !this.isAnonymos;
    }
  
    ngOnInit() {
      this.initForm();
      this.isAnonymos = false;
      this.userService.findByid(localStorage.getItem('userId')).subscribe( (value) => {
        this.user =  value["body"];
        if (this.type == "etudiant" && value["body"]["clas"]!=null){
        
        }
        else{
         
        }
     })
    }

    initForm() { 
      this.ideaForm = this.formBuilder.group({
        Title: ['', Validators.required],
        note: ['', Validators.required]  
      });
    }
    
    refresh(): void {
      window.location.reload();
    }

    TabReturn(length){
      return new Array(length);
    }

    onSubmitForm() {
      const formValue = this.ideaForm.value;
      const newIdea = new Idea(
        null,
        formValue['Title'],
        formValue['note'],
        0,
        0,
        false,
        this.isAnonymos,
        this.user,
        new Date(),
        new Date()
      );
      this.ideaService.create(newIdea).subscribe(
        (value) => { 
          this.MessageService.sendMessage("idea added")
          this.eventManager.broadcast({
            name: 'IdeaListModification',
            content: 'Add an idea'
        });
          this.clear()
        },
        (error) => { 
          this.clear()
          this.openError()
        })
    }

    openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
    }
  
  clear() {
    this.activeModal.dismiss('cancel');
  }
 
}
