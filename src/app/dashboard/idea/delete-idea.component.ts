import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { Idea } from '../models/idea.model';
import { UserPopupService } from '../services/user-popup.service';
import { IdeaService } from '../services/idea.service';
import { MessageService } from '../services/message.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
@Component({
  selector: 'app-delete-idea',
  templateUrl: './delete-idea.component.html',
  styleUrls: ['./delete-idea.component.scss']
})
export class DeleteIdeaComponent implements OnInit {
    ideaId:number
    idea:Idea;
    user:User;
    constructor(
        private MessageService: MessageService,
        private ideaService: IdeaService,
        public activeModal: NgbActiveModal,
        private modalService: NgbModal
        ,private router: Router,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
    }
    openError() {
        const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
        modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    refresh(): void {
        window.location.reload();
    }
     
    confirmDelete() {
        this.ideaService.delete(this.idea.id).subscribe(
            (value) => {
                this.clear()
                this.MessageService.sendMessage("idea deleted")
                this.eventManager.broadcast({
                    name: 'IdeaListModification',
                    content: 'Delete an idea'
                });
            },
            (error) => {
                this.clear()
                this.MessageService.sendMessage("error")
                this.openError()
            }
        );  
    }
}
