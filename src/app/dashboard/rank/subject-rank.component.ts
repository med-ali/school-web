import { Component, OnInit,ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { RankEtudiant } from '../models/RankEtudiant.model';
import { Clas } from '../models/class.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { FormControl } from '@angular/forms';
import { Subject } from '../models/subject.model';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { ClassService } from '../services/class.service';
import { DatePipe } from '@angular/common';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
@Component({
  selector: 'app-rank',
  templateUrl: './subject-rank.component.html',
  styleUrls: ['./subject-rank.component.scss']
})
export class RankByClassAndSubjectComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  user: User
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  exams : Array<Exam>
  queryField: FormControl = new FormControl();
  isAdmin:boolean;
  type:string;
  subjects:Subject[];
  selectedSubject:Subject;
  clas:string;
  notes:EtudiantSubjectNote[];
  eventSubscriber: Subscription;
  dateSelected:any;
  phone:string;
  address:string;
  classes:Clas[]
  dateSelectedMonth:any;
  moyByMonth:number;
  isMonth:boolean;
  rank:number;
  selectedClass:Clas;
  ranksEtudiants:RankEtudiant[];
   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];
   // array of all items to be paged
   private allItems: any[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private subjectService: SubjectService,
    private datePipe: DatePipe,
    private eventManager: JhiEventManager,
    private userService: UserService,private router: Router,
    private modalService: NgbModal,
    private classService: ClassService,
    private route: ActivatedRoute) { }
    displayedColumns = ['FirstName', 'Moy', 'Rank'];
    dataSource: MatTableDataSource<RankEtudiant>;
  ngOnInit() {
    this.isMonth = false;
    this.registerChangeInLocations()
    this.currentPage = 0
    if (localStorage.getItem('userRole')== "ROLE_ADMIN"){
      this.isAdmin = true
    }
    this.userService.findByid(localStorage.getItem('userId')).subscribe( (value) => {
       this.user =  value["body"];
       this.userLastName = value["body"]["lastName"]
       this.userFirstName = value["body"]["firstName"]
       this.userEmail = value["body"]["email"]
       this.type = value["body"]["type"]
       this.userRole = value["body"]["roles"][0]["name"]   
       this.exams = value["body"]["exams"]
       this.phone = value["body"]["phone"]
       this.address = value["body"]["address"]
       this.selectedClass = value["body"]["clas"]
       if (this.type == "etudiant" && value["body"]["clas"]!=null){
         this.clas = value["body"]["clas"]["name"]
        this.subjectService.findAllSubjectsByEudiantId(localStorage.getItem('userId')).subscribe( (value) => {
          if (value["body"]) {
            this.subjects = value["body"]["content"];
          }
       })
       }
       else{
        this.classService.findClassByProf(localStorage.getItem('userId'),0).subscribe( (value) => {
          if (value["body"]) {
            this.classes = value["body"]["content"];
          }
       })
       }
    })
    console.log(this.userRole+ ''+this.isAdmin)
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  getEtudiantNumber(clas:Clas){
    this.userService.getEtudiantNumber(clas.id).subscribe( (value) => {
      console.log(value)
        let idIndex = this.classes.indexOf(clas);
        clas.etudiantNumber = value;
     
   })
  }
  toMoy(num:any){
    return num.toFixed(2)
  }
  //pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
  setPageS(page: number) {
    // get pager object from service
    this.pager = this.subjectService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
}
  onDateSelected(){
    this.isMonth = false;
    this.queryByDate(0,this.dateSelected);
  }
  onDateSelectedMonth(){
    this.dateSelectedMonth = this.datePipe.transform(this.dateSelectedMonth,"yyyy-MM-dd");
    this.isMonth = true;
    this.queryByMonth(0,this.dateSelectedMonth);
  }
  loadAll(){
    this.subjectService.findSubjectsByEudiantId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => {
      this.subjects = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"] 
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      } 
    })
  }
  loadAllSubjectByClass(){
    this.subjectService.findAllSubjectsByClassId(this.selectedClass.id).subscribe( (value) => {
      this.subjects = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
    })
  }
  query(page:number){
    this.subjectService.findSubjectNoteByEudiantId(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,page).subscribe( (value) => {
      this.notes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
      })
  }
 
  queryByDate(page:number,date:any){
    this.subjectService.findSubjectNoteByEudiantIdByDate(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,page,date).subscribe( (value) => {
      this.notes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
      })
  }
  queryByMonth(page:number,date:any){
    this.subjectService.findSubjectRankByMonth(this.selectedClass.id,this.selectedSubject.id,date,page).subscribe( (value) => {
      this.ranksEtudiants = value["body"]["content"];
      this.allItems = value["body"]["content"];
      this.setPageS(1);
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }   
      })
      
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.isMonth){
        this.currentPage = page
        this.queryByMonth(page,this.dateSelectedMonth)
      }
      else{
        this.currentPage = page
         this.queryByDate(page,this.dateSelected)
      }   
    }  
  }
  previousState() {
    window.history.back();
  }
  onOptionsSelected(){
    this.isMonth = false ;
    console.log(this.selectedSubject.id+"subjects"+this.selectedSubject.title);
    /*this.subjectService.findSubjectNoteByEudiantId(this.route["snapshot"]["params"]["id"],this.selectedSubject.id,0).subscribe( (value) => {
      this.notes = value["body"]["content"];
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
      console.log(value["body"]["content"])
  })*/
  }
  onOptionsSelectedClass(){
    this.isMonth = false ;
    console.log(this.selectedClass.id+"class");
    this.loadAllSubjectByClass()
  }
   
  registerChangeInLocations() {
    this.eventSubscriber = this.eventManager.subscribe('NoteListModification', (response) => this.onOptionsSelected());
}
search(key:string){

}
}
