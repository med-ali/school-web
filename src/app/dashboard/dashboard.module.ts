import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'; 
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserListModule } from './user-list/user-list.module';
import { RankModule } from './rank/rank.module';
import { ClassModule } from './class/class.module';
import { IdeaModule } from './idea/idea.module';
import { QuoteModule } from './quote/quote.module';
import { SubjectModule } from './subject/subject.module';
import { ExamListModule } from './exam-list/exam-list.module';
import { NotFoundModule } from './four-oh-four/not-found.module'; 
import { QuestionListModule } from './question-list/question-list.module';
import { UserAnswerModule } from './user-answer/user-answer.module';
import { UserRoute  } from './user-list/user-list.route';
import { MODULE_ROUTES } from './dashboard.routes';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/shared.module';
import { UserAnswerComponent } from './user-answer/user-answer.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { NavBarComponent } from './nav-bar/nav-bar.component';
@NgModule({
  declarations: [
    DashboardComponent,
    UserAnswerComponent,
    NavBarComponent
  ],
  entryComponents: [
     
],
  imports: [ 
    BrowserModule,
    //AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,   
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forChild(MODULE_ROUTES),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,   
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    UserListModule,
    ExamListModule,
    ClassModule,
    RankModule,
    SubjectModule,
    SharedModule,
    IdeaModule,
    QuoteModule,
    QuestionListModule,
    MaterialModule,
    UserAnswerModule,
    NotFoundModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })

  ],
  providers: [
     
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [DashboardComponent]
})
export class DashboardModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}