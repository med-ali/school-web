import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import { Subject } from '../models/subject.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-new-subject',
  templateUrl: './new-subject.component.html'
})
export class NewSubjectComponent implements OnInit {
  subjectForm: FormGroup;
  queryField: FormControl = new FormControl();
  TabDate : Array<any> = []
  users : Array<any> = []
  ExamDate:Date
  userschecked : Array<any> = []
  totalItems: any;
  queryCount: any;
  page: any;
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number 
  constructor(private MessageService: MessageService,private modalService: NgbModal,private _searchService: SearchService ,private formBuilder: FormBuilder,
    private subjectService: SubjectService,
    private router: Router,private activeModal: NgbActiveModal) { }
    onChange(id:number, isChecked: boolean) {
      if(isChecked) {
        this.userschecked.push(id);
      } else {
        let index = this.userschecked.indexOf(id);
        this.userschecked.splice(index,1);
        this.TabDate.splice(index,1);
      } 
    }

    onChangeDate(id:number,date:any){ 
      if (this.userschecked.includes(id)){
        let index = this.userschecked.indexOf(id);
        this.TabDate.splice(index,1);
        this.TabDate.push(date) 
      }
    }
  
    ngOnInit() {
      this.initForm();
    }

    initForm() { 
      this.subjectForm = this.formBuilder.group({
        Title: ['', Validators.required],
        Description: ['', Validators.required]  
      });
    }
    
    refresh(): void {
      window.location.reload();
    }

    TabReturn(length){
      return new Array(length);
    }

    onSubmitForm() {
      const formValue = this.subjectForm.value;
      const newSubject = new Subject(
        null,
        formValue['Title'],
        formValue['Description'] 
      );
      this.subjectService.create(newSubject).subscribe(
        (value) => { 
          this.MessageService.sendMessage("subject added now")
          this.clear()
        },
        (error) => { 
          this.clear()
          this.openError()
        })
    }

    openError() {
      const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
    }

    /****** Search *******/  
    search(){
      this.currentPage = 0 
      if (this.queryField['value']){
          this._searchService.findAllByName(this.queryField['value'],0).subscribe(
            (value) => { 
              this.users = value["body"]["content"]; 
            this.users= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
            if (this.totalPages>6){
              this.pageView =  6
            }else{
              this.pageView =  this.totalPages
            } 
          });
          
      } 
    }
    
    setPage(page: number) {
      if ( page < this.totalPages  && 0 <= page   ){   
        if (this.queryField['value']){
          this.currentPage = page
          this._searchService.querySearch(this.queryField['value'],page).subscribe(  
            (value) => { 
              this.users= value["body"]["content"]; 
              this.totalElements=value["body"]["totalElements"]
              this.totalPages = value["body"]["totalPages"] 
            }
          );
        } 
      }  
    } 
  clear() {
    this.activeModal.dismiss('cancel');
  }
 
}
