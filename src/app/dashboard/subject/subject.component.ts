import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { PagerService } from  '../services/pager.service'; 
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import { Subject } from '../models/subject.model';
import { SubjectService } from '../services/subject.service';
import { UserService } from '../services/user.service';
//import { ITEMS_PER_PAGE, Principal } from '../../shared';
//import { Supplier, SupplierService } from '../supplier';
/*import { NewSubjectComponent } from './class-dialog.component';
import { DeleteClassComponent } from './delete-class.component';*/
import { EditSubjectComponent } from './edit-subject.component';
import { NewSubjectComponent } from './new-subject.component';
import { MessageService } from '../services/message.service';
import { DeleteSubjectComponent } from './delete-subject.component';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
    selector: 'jhi-class',
    templateUrl: './subject.component.html'
})


export class SubjectComponent implements OnInit, OnDestroy {
  
    totalItems: any;
  queryCount: any;
  page: any;
  subjects:Subject[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  compTitle:number = 1
  compDescription:number = 1
  subscription: Subscription;
  message: any;
  sortTitle:boolean = true;
  sortDesc:boolean = true;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
    constructor(private MessageService: MessageService,private activatedRoute: ActivatedRoute,
      private _searchService: SearchService,private modalService: NgbModal, 
      private subjectService: SubjectService ,private userService: UserService, 
      private pagerService: PagerService,private route: ActivatedRoute,private router: Router,
        ) {  
        this.subscription = this.MessageService.getMessage().subscribe(message => { 
          console.log("message"+message["text"])
          if (message["text"] == "subject added now" || message["text"] == "subject edited now"|| message["text"] == "subject deleted" )  
          {
            this.getAll()
            this.message = message;
          }
        });   
    }
    ngOnInit() {
        this.getAll()
        
    }
    
    ngOnDestroy() {
        
    }
   
    getAll(){
        this.currentPage = 0
        this.subjectService.query(0).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"];
            console.log(value["body"].length +"this.classes"+this.subjects)
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            if (this.totalPages>6){
              this.pageView =  6
            }else{
              this.pageView =  this.totalPages
            }
            //this.setPage(1);
          }
        );   
    }
    pagedItems: any[];
    TabReturn(length){
      return new Array(length);
    }
    openNew() {
        const modalRef = this.modalService.open( NewSubjectComponent, {windowClass:  'fadeStyle' , centered: true});
        //modalRef.componentInstance.clas = clas;
    }
    
    openDelete(s:any) {
      const modalRef = this.modalService.open( DeleteSubjectComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.s = s;
    }
  openEdit(s:any) {
    const modalRef = this.modalService.open( EditSubjectComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.s = s;
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        this._searchService.querySearch(this.queryField['value'],page).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }
      else{
        this.currentPage = page
        this.subjectService.query(page).subscribe(  
          (value) => { 
            this.subjects= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
}