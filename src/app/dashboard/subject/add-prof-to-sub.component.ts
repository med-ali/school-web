import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SearchService } from '../services/search.service';
import { User } from '../models/User.model';
import { FormControl } from '@angular/forms';
import { Subject } from '../models/subject.model';
import { ExamUser } from '../models/ExamUser.model';
import { ClassUser } from '../models/ClassUser.model';
import { SubjectService } from '../services/subject.service';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-add-user-to-sub',
  templateUrl: './add-prof-to-sub.component.html'
})
export class AddProfToSubjectComponent implements OnInit {

  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private router: Router,private activeModal: NgbActiveModal,
    private subjectService: SubjectService,
    private formBuilder: FormBuilder,
    private _searchService: SearchService,private userService: UserService) { }
  id:number
  subjectId:number
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  userClass:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  UsersChecked : Array<any> = []
  idUsersChecked : Array<any> = []
  TabDate : Array<any> = []
  sessionDate : any
  TabClassUser:ClassUser[]
  TabUserId:Number[]
  subject:Subject
    
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to add candidat !', status: false };
  }

  TabReturn(length){
    return new Array(length);
  }

  onChange(user:any, isChecked: boolean) {
    if(isChecked) {
      this.UsersChecked.push(user);
      this.idUsersChecked.push(user.id);
      console.log(user.id+"this.0idUsers"+"Checked"+this.idUsersChecked) 
    } else {
      let index = this.UsersChecked.indexOf(user);
      this.UsersChecked.splice(this.UsersChecked.indexOf(user),1); 
      let idIndex = this.idUsersChecked.indexOf(user.id);
      this.idUsersChecked.splice(this.idUsersChecked.indexOf(user.id),1); 
      console.log(user.id+"this.0idUsers"+"Checked"+this.idUsersChecked) 
    } 
  }

  isEmpty(Tab :any[]){
      let resultat:Boolean = false
      for (let e of Tab) {
        if (e===undefined) {
          resultat = true;
          break;
        }
      }
    return resultat;
  }

  onChangeDate(date:any){ 
    console.log(date)
    this.sessionDate = date
     
  }

  submit(subjectId:number){
    this.TabClassUser = []
    this.TabUserId = []
    this.UsersChecked.forEach(user => {
        const newClassUser = new ClassUser(
          user,
          this.subject,
          this.sessionDate        
        );   
        this.TabClassUser.push(newClassUser)
        this.TabUserId.push(user.id)
      });
      this.subjectService.createProfSubject(subjectId,this.TabUserId).subscribe(
        (value) => {
          this.clear()  
          this.MessageService.sendMessage("candidate added to subject")
        },
        (error) => {
          this.clear()
          this.openError()
      })
    /*this.TabClassUser = []
    console.log(this.sessionDate)
    this.UsersChecked.forEach(user => {
      const newClassUser = new ClassUser(
        user,
        this.class,
        this.sessionDate
      );   
      this.TabClassUser.push(newClassUser) 
    });
    this.classService.createClassUser(this.TabClassUser ).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("candidate added")
      },
      (error) => {
        this.clear()
        this.openError()
    })
    console.log("this.sessionDate" + this.TabClassUser[0].users.id)*/
  }
  
  findUserById(id:number){ 
    let user:User 
    for (let e of this.users) {
      if (e.id === id) {
        user = e ;
        break;
      }
    }
    return user;  
  }
  
  findUserExamById(id:number){ 
    let resultat:boolean=false 
    for (let e of this.TabClassUser) {
      if (e.users.id === id) {
        resultat = true
        break;
      }
    }
    return resultat;  
  }

  ngOnInit() {
    this.getAllProfNoinscrit();
    this.users= []
    this.currentPage = 0 
    this.TabClassUser = []
    this.sessionDate = "2019-07-30"
  }
  getAllProfNoinscrit(){
    this.userService.getAllProfsNoInscritBySubject(this.subjectId,0).subscribe(  
      (value) => {
        this.users= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
  search(){ 
    if (this.queryField['value']){
      this.users = []
      this.userService.getProfsSearchByLastNameNoInscritBySubject(this.subjectId,0,this.queryField['value']).subscribe(
        (value) => { 
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.users = []
      this.userService.getAllProfsNoInscrit(this.subjectId,0).subscribe(  
        (value) => {
          this.users= value["body"]["content"]
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  setPage(page: number) {
    if ( page < this.totalPages && 0 <= page   ){   
      if (this.queryField['value']){
        this.users = []
        this.currentPage = page
        this.userService.getProfsSearchByLastNameNoInscritBySubject(this.subjectId,page,this.queryField['value']).subscribe(  
          (value) => {
            this.users= value["body"]["content"] 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      else{
        this.currentPage = page
        this.users = []
        this.userService.getAllProfsNoInscritBySubject(this.subjectId,page).subscribe(  
          (value) => {
            this.users= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
          }
        );
      }   
    }  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  refresh(): void {
    window.location.reload();
  }
}
