import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../services/subject.service';
import { UserService } from '../services/user.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Subject } from '../models/subject.model';
import { FormControl } from '@angular/forms';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
import { AddProfToSubjectComponent } from './add-prof-to-sub.component';
import { DeleteSubjectComponent } from './delete-subject.component';
@Component({
  selector: 'app-prof-by-subject',
  templateUrl: './liste-prof-by-sub.component.html',
  styleUrls: ['./liste-prof-by-sub.component.scss']
})
export class ProfBySubjectComponent implements OnInit {
  user: User
  userid : number
  classId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  subject: Subject
  className : string
  classDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  usersClass : User[]
  queryField: FormControl = new FormControl();
  subscription: Subscription;
  message: any;
  title:string;
  description:string;
  profNumber:number;
  constructor(private MessageService: MessageService,
    private modalService: NgbModal ,private subjectService: SubjectService,
    private userService: UserService,private router: Router,private route: ActivatedRoute) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      if (message["text"]=="candidate added to subject"){
        this.getAll()
        this.message = message;
      }
      
     });  
   }
   
  ngOnInit() {
    this.getAll()
    this.subjectService.find(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
      this.subject = value;
      this.title = value["title"]
      this.description = value["description"]
      this.profNumber = value["profNumber"]
      this.userRole = localStorage.getItem('userRole')
   })
  }
  previousState() {
    window.history.back();
  }
  getAll(){
    this.currentPage = 0
    this.userService.findProfBySubjectId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => { 
      this.users = value["body"]["content"]
      this.usersClass = value["body"]["content"]
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"]
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      }
    }) 
  }
  refresh(): void {
    window.location.reload();
  }
  openNew() {
    const modalRef = this.modalService.open( AddProfToSubjectComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.subjectId = this.route["snapshot"]["params"]["id"];
    modalRef.componentInstance.subject = this.subject;
  }
  openDelete(user:any) { 
    const modalRef = this.modalService.open(DeleteSubjectComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = user;
    modalRef.componentInstance.subject = this.subject;
  }
  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        
      }
      else{
        this.currentPage = page
        this.userService.findProfBySubjectId(this.route["snapshot"]["params"]["id"],page).subscribe(  
          (value) => { 
            this.users = value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
}
