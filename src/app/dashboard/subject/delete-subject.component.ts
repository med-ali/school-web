import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/User.model';
import { UserPopupService } from '../services/user-popup.service';
import { SubjectService } from '../services/subject.service';
import { MessageService } from '../services/message.service';
import { Router } from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { JhiEventManager } from 'ng-jhipster';
import { Subject } from '../models/subject.model';
@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-subject.component.html',
  styleUrls: ['./delete-subject.component.scss']
})
export class DeleteSubjectComponent implements OnInit {
    subjectId:number
    s:Subject;
    user:User;
    subject:Subject;
    constructor(
        private MessageService: MessageService,
        private subjectService: SubjectService,
        public activeModal: NgbActiveModal,
        private modalService: NgbModal
        ,private router: Router,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
    }
    openError() {
        const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
        modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    refresh(): void {
        window.location.reload();
    }
     
    confirmDelete() {
        if (this.user && this.subject){
            this.subjectService.deleteSubjectProf(this.subject.id,this.user.id).subscribe(
                (value) => {
                  this.clear()
                  this.MessageService.sendMessage("subject deleted")
                },
                (error) => {
                    this.clear()
                    this.MessageService.sendMessage("error")
                    this.openError()
                }
              );
        }
        else{
            this.subjectService.delete(this.s.id).subscribe(
                (value) => {
                  this.clear()
                  this.MessageService.sendMessage("subject deleted")
                },
                (error) => {
                    this.clear()
                    this.MessageService.sendMessage("error")
                    this.openError()
                }
              );
        }
       
    }
}
