
import { JhiPaginationUtil } from 'ng-jhipster';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
//import { UserRouteAccessService } from '../../shared';
import { SubjectComponent } from './subject.component';
import { ProfBySubjectComponent } from './liste-prof-by-sub.component';
//import { ClassPopupComponent } from './class-dialog.component';
/*import { ArticleDetailComponent } from './article-detail.component';
import { ArticleDeletePopupComponent } from './article-delete-dialog.component';
import { ArticleAssignedComponent } from './article-assigned.component';
import { ArticleNormPopupComponent } from './article-norm.component';*/
@Injectable()
export class ClassResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const SubjectRoute: Routes = [
    { 
        path: 'subject',
        component: SubjectComponent,canActivate: [AuthGuardService,AdminGuardService]
    },
    { 
        path: 'subject/:id/prof',
        component: ProfBySubjectComponent,canActivate: [AuthGuardService]
    }
     /*, {
        path: 'article/:id',
        component: ArticleDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.article.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product/article/:productId',
        component: ArticleAssignedComponent,
        resolve: {
            'pagingParams': ArticleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.article.default'
        },
        canActivate: [UserRouteAccessService]
    }*/
];

export const classPopupRoute: Routes = [
    {
        /*path: 'class-new',
        component: ClassPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.class.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'*/
    }/*,
    {
        path: 'article/:id/norm',
        component: ArticleNormPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.article.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'article/:id/edit',
        component: ArticlePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.article.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'article/:id/delete',
        component: ArticleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.article.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }*/
];
