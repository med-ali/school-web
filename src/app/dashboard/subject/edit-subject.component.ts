import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,ActivatedRoute } from '@angular/router';
import { SubjectService } from '../services/subject.service';
import { Subject } from '../models/subject.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-edit-exam',
  templateUrl: './edit-subject.component.html',
  styleUrls: ['./edit-subject.component.scss']
})
export class EditSubjectComponent implements OnInit {
  subjectId:number
  subjectForm: FormGroup;
  s:Subject;
  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private formBuilder: FormBuilder,private subjectService: SubjectService,
    private router: Router,private route: ActivatedRoute,private activeModal: NgbActiveModal) {
      
  }

  ngOnInit() {
    this.initForm();
  }
  initForm() { 
    this.subjectForm = this.formBuilder.group({
      Title: ['', Validators.required],
      Description: ['', Validators.required]  
    });
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to edit !', status: false };
  }
  onSubmitForm(id:number) {
    const formValue = this.subjectForm.value;
    const newSubject = new Subject(
      this.s.id,
      formValue['Title'],
      formValue['Description']
    );
    this.subjectService.update(newSubject).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("subject edited now")
      },
      (error) => {
        this.clear()
        this.openError()
      })
  }
  refresh(): void {
    window.location.reload();
  }
}
