import { Question } from './Question.model';
export class Proposition {  
    constructor(
      public description: string,
      public isTrue: boolean,
      public userAnswer: boolean,
      public questionId:number,
      public userId:number
    ) {}
  }