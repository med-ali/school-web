import { Exam } from './Exam.model';
export class User {
   
    constructor(
      public id:number,
      public firstName: string,
      public lastName: string,
      public email: string,
      public password: string,
      public role:string,
      public type:string,
      public phone:string,
      public address:string
    ) {}
    
  }