import { Question } from './Question.model';
export class Answer {
    constructor( 
      public description: string,
      public note:number
    ) {}
}