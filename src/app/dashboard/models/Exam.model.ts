import { User } from './User.model';
export class Exam {
   
    constructor(
      public title: string,
      public description: string
    ) {}
  }