import { User } from './User.model';
import { Exam } from './Exam.model';
export class ExamUser {
    constructor(
      public users: User,
      public exam: Exam,
      public date:Date
    ) {}
  } 