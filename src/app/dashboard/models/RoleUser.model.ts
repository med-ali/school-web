import { User } from './User.model';
import { Role } from './Role.model';
export class RoleUser {
    constructor(
      public users: User,
      public role: Role
    ) {}
  } 