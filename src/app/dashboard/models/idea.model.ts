import { User } from './User.model';
export class Idea{
    constructor(
        public id?: number,
        public title?: string,
        public note?: string,
        public vote?: number,
        public star?: number,
        public deleted?:boolean,
        public isAnonymous?:boolean,
        public user?:User,
        public createdAt?:any,
        public updateAt?:any
    ) {
    }
}
