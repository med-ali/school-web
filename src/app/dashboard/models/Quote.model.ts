import { User } from './User.model';
export class Quote{
    constructor(   
        public createdAt?:any,
        public quote?: string,
        public updateAt?:any,    
        public user?:User,
        public id?: number
    ) {
    }
}