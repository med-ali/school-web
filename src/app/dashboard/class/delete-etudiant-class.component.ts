import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,ActivatedRoute } from '@angular/router';
import { ClassService } from '../services/class.service';
import { Clas } from '../models/class.model';
import { User } from '../models/User.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
import { Subject } from '../models/subject.model';
@Component({
  selector: 'app-delete-class',
  templateUrl: './delete-etudiant-class.component.html',
})
export class DeleteEtudiantClassComponent implements OnInit {
  clas: Clas;
  idC:number
  user: User;
  s:Subject;
  msg:string;
  constructor( private MessageService: MessageService,
      private classService: ClassService,
      public activeModal: NgbActiveModal,
      private modalService: NgbModal
      ,private router: Router
  ) {
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }
  ngOnInit() {
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  refresh(): void {
      window.location.reload();
  }

  confirmDelete() {
       if (this.user){  
      if (this.user.type == "etudiant" ){
        this.msg = "etudiant";
        console.log("etudiant")
        this.classService.deleteStudentClass(this.user.id).subscribe(
            (value) => { 
              this.clear()
              this.MessageService.sendMessage("class deleted")
            },
            (error) => {
              this.clear()
              this.openError()
            }
          );
      }
      else if (this.user.type == "prof") {
        this.msg = "prof";
        console.log("etudiant")
        this.classService.deleteProfClass(this.user.id,this.clas.id).subscribe(
            (value) => { 
              this.clear()
              this.MessageService.sendMessage("class deleted")
            },
            (error) => {
              this.clear()
              this.openError()
            }
          );  
      }
    }
    else {
      this.msg = "matiére";
      console.log("etudiant"+this.s.title)
      this.classService.deleteSubjectClass(this.s.id,this.idC).subscribe(
        (value) => { 
          this.clear()
          this.MessageService.sendMessage("class deleted")
        },
        (error) => {
          this.clear()
          this.openError()
        }
      );  
  }
    }
  }

