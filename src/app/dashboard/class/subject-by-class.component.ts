import { Component, OnInit } from '@angular/core';
import { ClassService } from '../services/class.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { Subject } from '../models/subject.model';
import { Clas } from '../models/class.model';
import { FormControl } from '@angular/forms';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
import { AddProfToClassComponent } from './add-prof-to-class.component';
import { DeleteEtudiantClassComponent } from './delete-etudiant-class.component';
import { AddSubjectToClassComponent } from './add-subject-to-class.component';
@Component({
  selector: 'app-subject-by-class',
  templateUrl: './subject-by-class.component.html',
  styleUrls: ['./subject-by-class.component.scss']
})
export class SubjectByClassComponent implements OnInit {
  subject: Subject
  userid : number
  classId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  clas: Clas
  className : string
  classDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  subjects:Subject[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  subjectsClass : Subject[]
  queryField: FormControl = new FormControl();
  subscription: Subscription;
  message: any;
  name:string;
  description:string;
  etudiantNumber:number;
  constructor(private MessageService: MessageService,private modalService: NgbModal 
    ,private classService: ClassService ,private subjectService: SubjectService,
    private router: Router,private route: ActivatedRoute) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      this.getAll()
      this.message = message;
     });  
   }
   
  ngOnInit() {
    this.getAll()
    this.classService.find(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
      this.clas = value;
      this.name = value["name"]
      this.description = value["description"]
      this.etudiantNumber = value["etudiantNumber"]
   })
  }
  previousState() {
    window.history.back();
  }
  getAll(){
    this.currentPage = 0
    this.subjectService.findSubjectsByClassId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => { 
      console.log(value["body"])
      console.log(value)
      this.subjects = value["body"]["content"]
      console.log("this.users"+value["body"]["content"].length )
      this.subjectsClass = value["body"]["content"]
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"]
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      }
    }) 
  }
  refresh(): void {
    window.location.reload();
  }
  openNew() {
    const modalRef = this.modalService.open( AddSubjectToClassComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.classId = this.route["snapshot"]["params"]["id"];
    modalRef.componentInstance.class = this.clas;
  }
  openDelete(s:any) { 
    const modalRef = this.modalService.open(DeleteEtudiantClassComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.s = s;
    modalRef.componentInstance.idC = this.route["snapshot"]["params"]["id"];
  }
  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        
      }
      else{
        this.currentPage = page
        this.subjectService.findSubjectsByClassId(this.route["snapshot"]["params"]["id"],page).subscribe(  
          (value) => { 
            this.subjects= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
}
