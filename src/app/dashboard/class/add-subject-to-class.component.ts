import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../services/subject.service';
import { SearchService } from '../services/search.service';
import { Subject } from '../models/subject.model';
import { FormControl } from '@angular/forms';
import { Clas } from '../models/class.model';
import { ExamUser } from '../models/ExamUser.model';
import { ClassUser } from '../models/ClassUser.model';
import { ClassService } from '../services/class.service';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-add-subject-to-class',
  templateUrl: './add-subject-to-class.component.html'
})
export class AddSubjectToClassComponent implements OnInit {

  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private router: Router,private activeModal: NgbActiveModal,private classService: ClassService,
    private formBuilder: FormBuilder,private _searchService: SearchService,private subjectService: SubjectService) {

      
     }
  id:number
  classId:number
  totalItems: any;
  queryCount: any;
  page: any;
  subjects:Subject[]
  userClass:Subject[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  SubjectsChecked : Array<any> = []
  idSubjectsChecked : Array<any> = []
  TabDate : Array<any> = []
  sessionDate : any
  TabClassUser:ClassUser[]
  TabSubjectId:Number[]
  class:Clas
    
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to add candidat !', status: false };
  }

  TabReturn(length){
    return new Array(length);
  }

  onChange(s:any, isChecked: boolean) {
    if(isChecked) {
      this.SubjectsChecked.push(s);
      this.idSubjectsChecked.push(s.id);
      console.log(s.id+"this.0idUsers"+"Checked"+this.idSubjectsChecked)
    } else {
      let index = this.SubjectsChecked.indexOf(s);
      this.SubjectsChecked.splice(this.SubjectsChecked.indexOf(s),1); 
      let idIndex = this.idSubjectsChecked.indexOf(s.id);
      this.idSubjectsChecked.splice(this.idSubjectsChecked.indexOf(s.id),1);
      console.log(s.id+"this.0idUsers"+"Checked"+this.idSubjectsChecked) 
    } 
  }

  isEmpty(Tab :any[]){
      let resultat:Boolean = false
      for (let e of Tab) {
        if (e===undefined) {
          resultat = true;
          break;
        }
      }
    return resultat;
  }

  onChangeDate(date:any){ 
    console.log(date)
    this.sessionDate = date
     
  }

  submit(classId:number){
    this.TabClassUser = []
    this.TabSubjectId = []
    this.SubjectsChecked.forEach(s => {
        const newClassUser = new ClassUser(
          s,
          this.class,
          this.sessionDate        
        );   
        this.TabClassUser.push(newClassUser)
        this.TabSubjectId.push(s.id)
        console.log(this.TabSubjectId+"lkl"+classId)
      });
      this.classService.createClassSubject(classId,this.TabSubjectId).subscribe(
        (value) => {
          this.clear()  
          this.MessageService.sendMessage("candidate added")
        },
        (error) => {
          this.clear()
          this.openError()
      })
    
  }
  
  findUserById(id:number){ 
    let user:Subject
    for (let e of this.subjects) {
      if (e.id === id) {
        user = e ;
        break;
      }
    }
    return user;  
  }
  
  findUserExamById(id:number){ 
    let resultat:boolean=false 
    for (let e of this.TabClassUser) {
      if (e.users.id === id) {
        resultat = true
        break;
      }
    }
    return resultat;  
  }

  ngOnInit() {
    this.getAllSubjectNoinscrit();
    this.subjects= []
    this.currentPage = 0 
    this.TabClassUser = []
    this.sessionDate = "2019-07-30"
  }
  getAllSubjectNoinscrit(){
    this.subjectService.getAllSubjectsNoInscritByClass(this.classId,0).subscribe(  
      (value) => {
        this.subjects= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
  search(){ 
    if (this.queryField['value']){
      this.subjects = []
      this.subjectService.getSubjectsSearchByLastNameNoInscritByClass(this.classId,0,this.queryField['value']).subscribe(
        (value) => { 
          this.subjects= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.subjects = []
      this.subjectService.getAllSubjectsNoInscritByClass(this.classId,0).subscribe(  
        (value) => {
          this.subjects= value["body"]["content"]
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  setPage(page: number) {
    if ( page < this.totalPages && 0 <= page   ){   
      if (this.queryField['value']){
        this.subjects = []
        this.currentPage = page
        this.subjectService.getSubjectsSearchByLastNameNoInscritByClass(this.classId,page,this.queryField['value']).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"] 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      else{
        this.currentPage = page
        this.subjects = []
        this.subjectService.getAllSubjectsNoInscritByClass(this.classId,page).subscribe(  
          (value) => {
            this.subjects= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
          }
        );
      }   
    }  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  refresh(): void {
    window.location.reload();
  }
}
