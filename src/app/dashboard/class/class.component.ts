import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { PagerService } from  '../services/pager.service'; 
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import { Clas } from '../models/class.model';
import { ClassService } from '../services/class.service';
import { UserService } from '../services/user.service';
//import { ITEMS_PER_PAGE, Principal } from '../../shared';
//import { Supplier, SupplierService } from '../supplier';
import { ClassDialogComponent } from './class-dialog.component';
import { DeleteClassComponent } from './delete-class.component';
import { EditClassComponent } from './edit-class.component';
import { MessageService } from '../services/message.service';
@Component({
    selector: 'jhi-class',
    templateUrl: './class.component.html'
})
export class ClassComponent implements OnInit, OnDestroy {

    totalItems: any;
  queryCount: any;
  page: any;
  classes:Clas[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  compTitle:number = 1
  compDescription:number = 1
  subscription: Subscription;
  message: any;
  sortTitle:boolean = true;
  sortDesc:boolean = true;

    constructor(private MessageService: MessageService,private activatedRoute: ActivatedRoute,private _searchService: SearchService,private modalService: NgbModal, private classService: ClassService ,private userService: UserService, private pagerService: PagerService,private route: ActivatedRoute,private router: Router,
        ) {  
        this.subscription = this.MessageService.getMessage().subscribe(message => { 
          if (message["text"] == "class added" || message["text"] == "class edited" || message["text"] == "class deleted")  
          {
            this.getAll()
            this.message = message;
          }
        });   
    }
    ngOnInit() {
        this.getAll()
    }
 
    ngOnDestroy() {
        
    }
   
    getAll(){
        this.currentPage = 0
        this.classService.query(0).subscribe(  
          (value) => {
            this.classes= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            if (this.totalPages>6){
              this.pageView =  6
            }else{
              this.pageView =  this.totalPages
            }
            //this.setPage(1);
          }
        );   
    }
    getEtudiantNumber(clas:Clas){
      this.userService.getEtudiantNumber(clas.id).subscribe( (value) => {
        console.log(value)
          let idIndex = this.classes.indexOf(clas);
          clas.etudiantNumber = value;
       
     })
    }
  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
    openNew(clas:any) {
        const modalRef = this.modalService.open( ClassDialogComponent, {windowClass:  'fadeStyle' , centered: true});
        modalRef.componentInstance.clas = clas;
    }
    openDelete(clas:any) {
      const modalRef = this.modalService.open( DeleteClassComponent, {windowClass:  'fadeStyle' , centered: true});
      modalRef.componentInstance.clas = clas;
  }
  openEdit(clas:any) {
    const modalRef = this.modalService.open( EditClassComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.clas = clas;
}
setPage(page: number) {
  if ( page < this.totalPages  && 0 <= page   ){   
    if (this.queryField['value']){
      this.currentPage = page
      this._searchService.querySearch(this.queryField['value'],page).subscribe(  
        (value) => {
          this.classes= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
        }
      );
    }
    else{
      this.currentPage = page
      this.classService.query(page).subscribe(  
        (value) => { 
          this.classes= value["body"]["content"]; 
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
        }
      );
    }   
  }  
}
}