import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SearchService } from '../services/search.service';
import { User } from '../models/User.model';
import { FormControl } from '@angular/forms';
import { Clas } from '../models/class.model';
import { ExamUser } from '../models/ExamUser.model';
import { ClassUser } from '../models/ClassUser.model';
import { ClassService } from '../services/class.service';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-add-user-to-class',
  templateUrl: './add-etudiant-to-class.component.html'
})
export class AddUserToClassComponent implements OnInit {

  constructor(private MessageService: MessageService,private modalService: NgbModal,
    private router: Router,private activeModal: NgbActiveModal,private classService: ClassService,
    private formBuilder: FormBuilder,private _searchService: SearchService,private userService: UserService) { }
  id:number
  classId:number
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  userClass:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  UsersChecked : Array<any> = []
  idUsersChecked : Array<any> = []
  TabDate : Array<any> = []
  sessionDate : any
  TabClassUser:ClassUser[]
  TabUserId:Number[]
  class:Clas
    
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to add candidat !', status: false };
  }

  TabReturn(length){
    return new Array(length);
  }

  onChange(user:any, isChecked: boolean,i:number) {
    if(isChecked) {
      this.UsersChecked.push(user);
      this.idUsersChecked.push(user.id);
      console.log(user.id+"this.1idUsers"+i+"Checked"+this.idUsersChecked)
    } else {
      let index = this.UsersChecked.indexOf(user);
      this.UsersChecked.splice(this.UsersChecked.indexOf(user),1);
      let idIndex = this.idUsersChecked.indexOf(user);
      this.idUsersChecked.splice(this.idUsersChecked.indexOf(user.id),1);
      console.log(user.id+"this.0idUsers"+i+"Checked"+this.idUsersChecked)
    }  
  }

  isEmpty(Tab :any[]){
      let resultat:Boolean = false
      for (let e of Tab) {
        if (e===undefined) {
          resultat = true;
          break;
        }
      }
    return resultat;
  }

  onChangeDate(date:any){ 
    console.log(date)
    this.sessionDate = date
     
  }

  submit(classId:number){
    this.TabClassUser = []
    this.TabUserId = []
    this.UsersChecked.forEach(user => {
        const newClassUser = new ClassUser(
          user,
          this.class,
          this.sessionDate        
        );   
        this.TabClassUser.push(newClassUser)
        this.TabUserId.push(user.id)
        console.log(this.TabUserId+"lkl"+classId)
      });
      this.classService.createClassStudent(classId,this.TabUserId).subscribe(
        (value) => {
          this.clear()  
          this.MessageService.sendMessage("candidate added")
        },
        (error) => {
          this.clear()
          this.openError()
      })
    /*this.TabClassUser = []
    console.log(this.sessionDate)
    this.UsersChecked.forEach(user => {
      const newClassUser = new ClassUser(
        user,
        this.class,
        this.sessionDate
      );   
      this.TabClassUser.push(newClassUser) 
    });
    this.classService.createClassUser(this.TabClassUser ).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("candidate added")
      },
      (error) => {
        this.clear()
        this.openError()
    })
    console.log("this.sessionDate" + this.TabClassUser[0].users.id)*/
  }
  
  findUserById(id:number){ 
    let user:User 
    for (let e of this.users) {
      if (e.id === id) {
        user = e ;
        break;
      }
    }
    return user;  
  }
  
  findUserExamById(id:number){ 
    let resultat:boolean=false 
    for (let e of this.TabClassUser) {
      if (e.users.id === id) {
        resultat = true
        break;
      }
    }
    return resultat;  
  }

  ngOnInit() {
    this.getAllUserNoinscrit();
    this.users= []
    this.currentPage = 0 
    this.TabClassUser = []
    this.sessionDate = "2019-07-30"
  }
  getAllUserNoinscrit(){
    this.userService.getAllUsersNoInscritByClass(this.classId,0).subscribe(  
      (value) => {
        this.users= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
  search(){ 
    if (this.queryField['value']){
      this.users = []
      this.userService.getStudentsSearchByLastNameNoInscritByClass(this.classId,0,this.queryField['value']).subscribe(
        (value) => { 
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.users = []
      this.userService.getAllStudentsNoInscrit(this.classId,0).subscribe(  
        (value) => {
          this.users= value["body"]["content"]
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  setPage(page: number) {
    if ( page < this.totalPages && 0 <= page   ){   
      if (this.queryField['value']){
        this.users = []
        this.currentPage = page
        this.userService.getStudentsSearchByLastNameNoInscritByClass(this.classId,page,this.queryField['value']).subscribe(  
          (value) => {
            this.users= value["body"]["content"] 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      else{
        this.currentPage = page
        this.users = []
        this.userService.getAllStudentsNoInscrit(this.classId,page).subscribe(  
          (value) => {
            this.users= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
          }
        );
      }   
    }  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  refresh(): void {
    window.location.reload();
  }
}
