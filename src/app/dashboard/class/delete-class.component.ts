import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,ActivatedRoute } from '@angular/router';
import { ClassService } from '../services/class.service';
import { Clas } from '../models/class.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-delete-class',
  templateUrl: './delete-class.component.html',
})
export class DeleteClassComponent implements OnInit {
  clas: Clas;
  classId:number
  constructor( private MessageService: MessageService,
      private classService: ClassService,
      public activeModal: NgbActiveModal,
      private modalService: NgbModal
      ,private router: Router
  ) {
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }
  ngOnInit() {
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  refresh(): void {
      window.location.reload();
  }

  confirmDelete() {
    this.classService.delete(this.clas.id).subscribe(
      (value) => { 
        this.clear()
        this.MessageService.sendMessage("class deleted")
      },
      (error) => {
        this.clear()
        this.openError()
      }
    );
  }
}
