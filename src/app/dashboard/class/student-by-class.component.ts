import { Component, OnInit } from '@angular/core';
import { ClassService } from '../services/class.service';
import { UserService } from '../services/user.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Clas } from '../models/class.model';
import { FormControl } from '@angular/forms';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
import { AddUserToClassComponent } from './add-etudiant-to-class.component';
import { DeleteEtudiantClassComponent } from './delete-etudiant-class.component';
@Component({
  selector: 'app-student-by-class',
  templateUrl: './student-by-class.component.html',
  styleUrls: ['./student-by-class.component.scss']
})
export class StudentByClassComponent implements OnInit {
  user: User
  userid : number
  classId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  clas: Clas
  className : string
  classDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  usersClass : User[]
  queryField: FormControl = new FormControl();
  subscription: Subscription;
  message: any;
  name:string;
  description:string;
  etudiantNumber:number;
  constructor(private MessageService: MessageService,private modalService: NgbModal ,private classService: ClassService ,private userService: UserService,private router: Router,private route: ActivatedRoute) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      this.getAll()
      this.message = message;
     });  
   }
   
  ngOnInit() {
    this.getAll()
    this.classService.find(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
      this.clas = value;
      this.name = value["name"]
      this.description = value["description"]
      this.etudiantNumber = value["etudiantNumber"]
   })
  }
  getAll(){
    this.currentPage = 0
    this.userService.findStudentByClassId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => { 
      this.users = value["body"]["content"]
      this.usersClass = value["body"]["content"]
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"]
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      }
    }) 
  }
  refresh(): void {
    window.location.reload();
  }
  previousState() {
    window.history.back();
  }
  openNew() {
    const modalRef = this.modalService.open( AddUserToClassComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.classId = this.route["snapshot"]["params"]["id"];
    modalRef.componentInstance.class = this.clas;
    modalRef.componentInstance.userExam = this.usersClass;
  }
  openDelete(user:any) { 
    const modalRef = this.modalService.open(DeleteEtudiantClassComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.user = user;
    modalRef.componentInstance.clas = this.clas;
  }
  pagedItems: any[];
  TabReturn(length){
    return new Array(length);
  }
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
        this.currentPage = page
        
      }
      else{
        this.currentPage = page
        this.userService.findStudentByClassId(this.route["snapshot"]["params"]["id"],page).subscribe(  
          (value) => { 
            this.users = value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"] 
          }
        );
      }   
    }  
  }
}
