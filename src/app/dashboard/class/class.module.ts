 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
//import { ProfilComponent } from './'; 
import {ClassRoute  } from './class.route';
//import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DeleteUserComponent,UserDeletePopupComponent  } from './delete-user.component';
//import {  EditUserComponent } from './edit-user.component';
import {MatDialogModule} from '@angular/material';
//import {  NewUserComponent } from './new-user.component';
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { ClassComponent  } from './class.component';
import { ClassDialogComponent  } from './class-dialog.component';
import { EditClassComponent  } from './edit-class.component';
import { DeleteClassComponent } from './delete-class.component';
import { StudentByClassComponent } from './student-by-class.component';
import { AddUserToClassComponent } from './add-etudiant-to-class.component';
import { DeleteEtudiantClassComponent } from './delete-etudiant-class.component';
import { ProfByClassComponent } from './prof-by-class.component';
import { AddProfToClassComponent } from './add-prof-to-class.component';
import { SubjectByClassComponent } from './subject-by-class.component';
import { AddSubjectToClassComponent } from './add-subject-to-class.component';
const ENTITY_STATES = [
    ...ClassRoute
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
        
    ],
    declarations: [
        ClassComponent,
        ClassDialogComponent,
        EditClassComponent,
        DeleteClassComponent,
        StudentByClassComponent,
        AddUserToClassComponent,
        DeleteEtudiantClassComponent,
        ProfByClassComponent,
        AddProfToClassComponent,
        SubjectByClassComponent,
        AddSubjectToClassComponent
    ],
    entryComponents: [
        ClassDialogComponent,
        EditClassComponent,
        DeleteClassComponent,
        AddUserToClassComponent,
        DeleteEtudiantClassComponent,
        AddProfToClassComponent,
        SubjectByClassComponent,
        AddSubjectToClassComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClassModule { }
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}