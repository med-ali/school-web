import { Component, OnInit } from '@angular/core';
import { ExamService } from '../services/exam.service';
import { UserService } from '../services/user.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-exam-detail',
  templateUrl: './exam-detail.component.html',
  styleUrls: ['./exam-detail.component.scss']
})
export class ExamDetailComponent implements OnInit {
  user: User
  userid : number
  examId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  exam: Exam
  examTitle : string
  examDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  usersExam : User[]
  queryField: FormControl = new FormControl();
  subscription: Subscription;
  message: any;
  constructor(private MessageService: MessageService,private modalService: NgbModal ,private examService: ExamService ,private userService: UserService,private router: Router,private route: ActivatedRoute) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      this.getAll()
      this.message = message;
     });  
   }
   
  ngOnInit() { 
    this.getAll()
    
  }
  getAll(){
    this.examService.findByid(this.route["snapshot"]["params"]["id"]).subscribe( (value) => { 
      this.exam = value["body"]["exam"] 
      this.examTitle = value["body"]["exam"]["title"]
      this.examDescription = value["body"]["exam"]["description"]
      this.examId = value["body"]["exam"]["id"]  
      this.usersExam = value["body"]["users"]["content"]  
    }) 
  }
  refresh(): void {
    window.location.reload();
  }
  /*openDelete(userId:number) { 
    const modalRef = this.modalService.open(DeleteExamUserRegistrationComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.userId = userId;
    modalRef.componentInstance.examId = this.examId;
  }
  
  openNew() {
    const modalRef = this.modalService.open( AddUserToExamComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.examId = this.route["snapshot"]["params"]["id"];
    modalRef.componentInstance.exam = this.exam;
    modalRef.componentInstance.userExam = this.usersExam;
  }*/
}
