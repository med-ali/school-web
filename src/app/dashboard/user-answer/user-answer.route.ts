import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router'; 
import { UserAnswerComponent  } from './user-answer.component';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
export const UserAnswerRoute: Routes = [
    { 
        path: 'result/exam/:examId/user/:userId',
        component: UserAnswerComponent, canActivate: [AuthGuardService,AdminGuardService]
    }
];

export const UserAnswerPopupRoute: Routes = [
     
];