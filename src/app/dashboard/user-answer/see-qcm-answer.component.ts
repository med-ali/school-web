import { Component, OnInit } from '@angular/core';
import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../services/search.service';
import { QuestionService } from '../services/question.service'; 
import { AnswerService } from '../../dashboard/services/answer.service';
import { AlertComponent } from '../../shared/alert/alert.component'; 
import { Answer } from '../../dashboard/models/Answer.model';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms'; 
@Component({
  selector: 'app-see-qcm-answer',
  templateUrl: './see-qcm-answer.component.html',
  styleUrls: ['./see-qcm-answer.component.scss']
})
export class SeeQcmAnswerComponent implements OnInit {

  noteForm: FormGroup;
  examId:number
  userId:number
  questionId:number 
  answers:Answer[]
  answerId:number
  questionResponse:string
  questionScore:number
  
  constructor(private activeModal: NgbActiveModal,private formBuilder: FormBuilder,private answerService: AnswerService,private activatedRoute: ActivatedRoute,private questionService: QuestionService,private modalService: NgbModal
    ,private route: ActivatedRoute,private router: Router,
   ) {     
  }

  ngOnInit() { 
  }
 clear() {
    this.activeModal.dismiss('cancel');
  }

}
