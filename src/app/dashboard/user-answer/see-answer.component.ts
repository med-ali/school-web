import { Component, OnInit } from '@angular/core';
import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../services/search.service';
import { QuestionService } from '../services/question.service'; 
import { AnswerService } from '../../dashboard/services/answer.service';
import { AlertComponent } from '../../shared/alert/alert.component'; 
import { Answer } from '../../dashboard/models/Answer.model';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-see-answer',
  templateUrl: './see-answer.component.html',
  styleUrls: ['./see-answer.component.scss']
})
export class SeeAnswerComponent implements OnInit {
  noteForm: FormGroup;
  examId:number
  userId:number
  questionId:number 
  answer:Answer 
  answerId:number
  questionResponse:string
  questionScore:number
  
  constructor(private MessageService: MessageService,private activeModal: NgbActiveModal,private formBuilder: FormBuilder,private answerService: AnswerService,private activatedRoute: ActivatedRoute,private questionService: QuestionService,private modalService: NgbModal
    ,private route: ActivatedRoute,private router: Router,
   ) {     
  }

  ngOnInit() {
    this.initForm();
    console.log("examId")
    console.log(this.examId)
    console.log(this.userId)
  }

  initForm() { 
    this.noteForm = this.formBuilder.group({   
      note: ['', Validators.compose([Validators.required, Validators.max(this.questionScore), Validators.min(0)])]  
    });
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  onSubmitForm() { 
    const formValue = this.noteForm.value;
    const newAnswer = new Answer( 
      this.answer.description,
      formValue['note'] 
    );
    console.log(newAnswer)
    console.log(this.answerId)
    this.answerService.editAnswer(this.userId,this.answerId,newAnswer).subscribe(
      (value) => { 
        this.MessageService.sendMessage("score added");
        this.clear()
      },
      (error) => { 
        this.clear()
        this.openError()
    })
  }

  refresh(): void {
    window.location.reload();
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
