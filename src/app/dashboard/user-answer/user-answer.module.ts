 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core'; 
import {UserAnswerRoute  } from './user-answer.route';
import {UserAnswerPopupRoute } from './user-answer.route'; 
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {MatDialogModule} from '@angular/material';  
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { MaterialModule } from '../../shared/shared.module';
import { UserAnswerComponent  } from './user-answer.component';
import { SeeAnswerComponent } from './see-answer.component';
import { SeeQcmAnswerComponent } from './see-qcm-answer.component'; 

const ENTITY_STATES = [
    ...UserAnswerRoute,
    ...UserAnswerPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule, 
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule, 
        MaterialModule 
    ],
    declarations: [ 
        SeeAnswerComponent, SeeQcmAnswerComponent       
    ],
    entryComponents: [
       SeeAnswerComponent,
       SeeQcmAnswerComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserAnswerModule { }
 