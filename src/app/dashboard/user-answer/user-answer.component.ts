import { Component, OnInit } from '@angular/core';
import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../services/search.service';
import { QuestionService } from '../services/question.service';
import { SeeAnswerComponent } from './see-answer.component'; 
import { SeeQcmAnswerComponent } from './see-qcm-answer.component';
import { AnswerService } from '../../dashboard/services/answer.service';
import { UserService } from '../services/user.service';
import { ExamService } from '../../dashboard/services/exam.service';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-answer',
  templateUrl: './user-answer.component.html',
  styleUrls: ['./user-answer.component.scss']
})
export class UserAnswerComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  questions:Question[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  examId:number
  userId:number
  Iduser:number
  queryField: FormControl = new FormControl();
  compTitle:number = 0
  compDescription:number = 0 
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  exams : Array<Exam>
  score:number
  subscription: Subscription;
  message: any;
  constructor(private MessageService: MessageService,private examService: ExamService ,private userService: UserService,private answerService: AnswerService,private activatedRoute: ActivatedRoute,private questionService: QuestionService,private modalService: NgbModal
    ,private route: ActivatedRoute,private router: Router,
   ) {   
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      this.examId = this.route["snapshot"]["params"]["id"] 
      this.getAll()
      this.message = message;
     });    
  }

  ngOnInit() {
   this.getAll()
  }
  getAll(){
    this.examId = this.route["snapshot"]["params"]["examId"] 
    this.userId = this.route["snapshot"]["params"]["userId"] 
    this.currentPage = 0 
    this.getQuestions() 
    this.getProfil(this.userId)
    this.getScore(this.userId,this.examId)
  }
  getProfil(userId:number){
    this.userService.findByid(userId).subscribe( (value) => {
      this.userLastName = value["body"]["lastName"]
      this.userFirstName = value["body"]["firstName"]
      this.userEmail = value["body"]["email"]
      this.userRole = value["body"]["role"]["name"]   
      this.exams = value["body"]["exams"]
    })
  }
  getScore(userId:number,examId:number){
    this.examService.findExamUser(userId,examId).subscribe(  
      (value) => {
        console.log(value["body"]["score"])
        if (value["body"]["score"]==null){
          this.score = 0
        }else
        {
          this.score =  value["body"]["score"]
        }
      }
    );   
  }
  getQuestions(){   
    this.questions = []
    this.questionService.findAllByExam(this.examId,0).subscribe(  
      (value) => {
        this.questions= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }

  TabReturn(length){
    return new Array(length);
  }

  seeAnswer(userId:number,examId:number,questionId:number,type:string,questionResponse:string,questionScore:number){
    if (type == "qroQuestion"){
      this.answerService.getAnswerByquestionAndUser(this.userId,questionId).subscribe(
        (value) => {      
          const modalRef = this.modalService.open( SeeAnswerComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.examId = examId;
          modalRef.componentInstance.userId = userId;
          modalRef.componentInstance.questionId = questionId; 
          modalRef.componentInstance.questionResponse = questionResponse; 
          modalRef.componentInstance.questionScore = questionScore; 
          if (value["body"][0])
          { 
            modalRef.componentInstance.answer = value["body"][0];
            modalRef.componentInstance.answerId = value["body"][0]["id"];
          }   
        },
        (error) => {  
        }) 
    } 
    if (type == "qcmQuestion"){
      this.answerService.getAnswerByquestionAndUser(this.userId,questionId).subscribe(
        (value) => {  
          console.log('value["body"]') 
          console.log(value["body"])     
          const modalRef = this.modalService.open( SeeQcmAnswerComponent, {windowClass:  'fadeStyle' , centered: true});
          modalRef.componentInstance.examId = examId;
          modalRef.componentInstance.userId = userId;
          modalRef.componentInstance.questionId = questionId; 
          modalRef.componentInstance.questionResponse = questionResponse; 
          modalRef.componentInstance.questionScore = questionScore; 
          if (value["body"])
          {  
            modalRef.componentInstance.answers = value["body"];
            //modalRef.componentInstance.answerId = value["body"][;
          }   
        },
        (error) => {  
        }) 
    } 
  }
/******************Pagination *****************/
  setPage(page: number) {
    if ( page < this.totalPages  && 0 <= page   ){   
      if (this.queryField['value']){
      }
      else{
        this.currentPage = page
        this.questionService.findAllByExam(this.examId,page).subscribe(  
          (value) => { 
            this.questions= value["body"]["content"];
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      
    }  
  }
/******************Pagination *****************/

/****************** Sort *****************/
sort_by_title(key:string){
  if (this.queryField['value']){
    
  }
  else
  {
    if (this.compTitle % 2 == 0){
      this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
        (value) => { 
          console.log(value)
          this.questions = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      );
    }
    else{
      this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
        (value) => { 
          this.questions = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      ); 
    }
  }
  this.compTitle = this.compTitle + 1
}

sort_by_description(key:string){
  if (this.queryField['value']){
     
  }
  else
  {
    if (this.compDescription % 2 == 0){
      this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
        (value) => { 
          console.log(value)
          this.questions = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      );
    }
    else{
      this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
        (value) => { 
          this.questions = value["body"]["content"]; 
          this.totalElements = value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]       
        }
      ); 
    }
  }
  this.compDescription = this.compDescription + 1
}
}
