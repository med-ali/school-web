import { Component, OnInit } from '@angular/core';
import { Exam } from '../models/Exam.model';
import { Question } from '../models/Question.model';
import { FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../services/search.service';
import { QuestionService } from '../services/question.service';
import { NewQuestionComponent } from './new-question.component';
import { DeleteQuestionComponent } from './delete-question.component';
import { EditQuestionComponent } from './edit-question.component';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs'; 
@Component({ 
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  questions:Question[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  examId:number
  queryField: FormControl = new FormControl();
  compTitle:number = 0
  compDescription:number = 0
  subscription: Subscription;
  message: any;
  constructor(private MessageService: MessageService,private activatedRoute: ActivatedRoute,private questionService: QuestionService,private modalService: NgbModal
     ,private route: ActivatedRoute,private router: Router,
    ) { 
      this.subscription = this.MessageService.getMessage().subscribe(message => { 
        this.examId = this.route["snapshot"]["params"]["id"] 
        this.getQuestions()
        this.message = message;
       });     
  }

  ngOnInit() { 
    this.getQuestions()   
  }
  openNew(examId:number) {
    const modalRef = this.modalService.open( NewQuestionComponent, {windowClass:  'fadeStyle' , centered: true});
     modalRef.componentInstance.examId = this.route["snapshot"]["params"]["id"] ;
  }

  openEdit(examId:number,questionId:number) {
    const modalRef = this.modalService.open(EditQuestionComponent, {windowClass:  'fadeStyle' , centered: true});
     modalRef.componentInstance.examId = this.route["snapshot"]["params"]["id"] ;
     modalRef.componentInstance.questionId = questionId;
  }

  openDelete(questionId:number) {
    const modalRef = this.modalService.open(DeleteQuestionComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.examId = this.route["snapshot"]["params"]["id"] ;
    modalRef.componentInstance.questionId = questionId; 
  }
  
  TabReturn(length){
    return new Array(length);
  }

  getQuestions(){       
    this.currentPage = 0 
    this.questions = []
    this.questionService.findAllByExam(this.route["snapshot"]["params"]["id"],0).subscribe(  
      (value) => {
        this.questions= value["body"]["content"]
        console.log(value)
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
/******************Pagination *****************/
setPage(page: number) {
  if ( page < this.totalPages  && 0 <= page   ){   
    if (this.queryField['value']){
    }
    else{
      this.currentPage = page
      this.questionService.findAllByExam(this.route["snapshot"]["params"]["id"],page).subscribe(  
        (value) => { 
          this.questions= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          //this.setPage(1);
        }
      );
    }
     
  }  
}
/******************Pagination *****************/
/****************** Sort *****************/
  sort_by_title(key:string){
    if (this.queryField['value']){
      
    }
    else
    {
      if (this.compTitle % 2 == 0){
        this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            console.log(value)
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compTitle = this.compTitle + 1
  }

  sort_by_description(key:string){
    if (this.queryField['value']){
       
    }
    else
    {
      if (this.compDescription % 2 == 0){
        this.questionService.sortAsc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            console.log(value)
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.questionService.sortDesc(key,this.currentPage,this.examId).subscribe( 
          (value) => { 
            this.questions = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compDescription = this.compDescription + 1
  }
}
