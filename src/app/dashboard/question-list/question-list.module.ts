 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
//import { ProfilComponent } from './';
import {QuestionRoute  } from './question-list.route';
import {QuestionPopupRoute } from './question-list.route';
//import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DeleteUserComponent,UserDeletePopupComponent  } from './delete-user.component';
//import {  EditUserComponent } from './edit-user.component';
import {MatDialogModule} from '@angular/material';
import { NewQuestionComponent } from './new-question.component';
import { QuestionListComponent } from './question-list.component';
//import {  NewUserComponent } from './new-user.component';
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { DeleteQuestionComponent } from './delete-question.component';
import { EditQuestionComponent } from './edit-question.component';
import { MaterialModule } from '../../shared/shared.module';
import { EditQcmPropositionComponent } from './edit-qcm-proposition.component';
import { ListQcmPropositionComponent } from './list-qcm-proposition.component';
import { NewQcmPropositionComponent } from './new-qcm-proposition.component';
import { DeleteQcmPropositionComponent } from './delete-qcm-proposition.component';

const ENTITY_STATES = [
    ...QuestionRoute,
    ...QuestionPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule, 
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule, 
        MaterialModule 
    ],
    declarations: [    
        NewQuestionComponent, 
        QuestionListComponent, DeleteQuestionComponent, EditQuestionComponent, EditQcmPropositionComponent, ListQcmPropositionComponent, NewQcmPropositionComponent, DeleteQcmPropositionComponent    
    ],
    entryComponents: [
        NewQuestionComponent , 
        DeleteQuestionComponent,
        EditQuestionComponent,
        NewQcmPropositionComponent,
        EditQcmPropositionComponent, 
        DeleteQcmPropositionComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QuestionListModule { }
 