import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Question } from '../models/Question.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { QuestionService } from '../services/question.service';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-delete-question',
  templateUrl: './delete-question.component.html',
  styleUrls: ['./delete-question.component.scss']
})
export class DeleteQuestionComponent implements OnInit {
  questionId:number
  constructor(private MessageService: MessageService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private questionService: QuestionService,private activeModal: NgbActiveModal,private examService: ExamService ) { }

  ngOnInit() {
  } 

  refresh(): void {
    window.location.reload();
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete question!', status: false };
  }

  confirmDelete(id: number) {
    this.questionService.delete(id).subscribe(
      (value) => { 
        this.clear()  
        this.MessageService.sendMessage("question deleted")
      },
      (error) => { 
        this.clear()
        this.openError()
      }
    );
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
