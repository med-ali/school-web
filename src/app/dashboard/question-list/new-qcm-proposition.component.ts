import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Question } from '../models/Question.model';
import {  Proposition } from '../models/Proposition.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { QuestionService } from '../services/question.service';
import { PropositionService } from '../services/proposition.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-new-qcm-proposition',
  templateUrl: './new-qcm-proposition.component.html',
  styleUrls: ['./new-qcm-proposition.component.scss']
})
export class NewQcmPropositionComponent implements OnInit {
  propositionForm: FormGroup;
  questionId:number
  constructor(private MessageService: MessageService,private route: ActivatedRoute,private router: Router,private propositionService: PropositionService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private questionService: QuestionService,private activeModal: NgbActiveModal,private examService: ExamService ) { }

  ngOnInit() {
    this.initForm(); 
  }
  
  initForm() { 
    this.propositionForm = this.formBuilder.group({ 
      Description: ['', Validators.required], 
      isTrue: ['']
    });
  }

  onSubmitForm() { 
    const formValue = this.propositionForm.value;
    const newProposition = new Proposition( 
      formValue['Description'] ,
      formValue['isTrue'],
      false,
      null,
      null
    ); 
    this.propositionService.create(this.questionId,newProposition ).subscribe(
    (value) => {
      this.clear()
      this.MessageService.sendMessage("proposition added") 
    },
    (error) => {
      this.clear()
      this.openError()
    })
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  refresh(): void {
    window.location.reload();
  }
}
