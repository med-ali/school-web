import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router ,ActivatedRoute} from '@angular/router';
import { Question } from '../models/Question.model';
import { Proposition } from '../models/Proposition.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { QuestionService } from '../services/question.service';
import { PropositionService } from '../services/proposition.service';
import { NewQcmPropositionComponent } from './new-qcm-proposition.component';
import { EditQcmPropositionComponent } from './edit-qcm-proposition.component';
import { DeleteQcmPropositionComponent } from './delete-qcm-proposition.component';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-list-qcm-proposition',
  templateUrl: './list-qcm-proposition.component.html',
  styleUrls: ['./list-qcm-proposition.component.scss']
})
export class ListQcmPropositionComponent implements OnInit { 
  question:Question
  questionId:number
  propositions:Proposition[]
  subscription: Subscription;
  message: any;
  constructor(private MessageService: MessageService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal,private formBuilder: FormBuilder,private propositionService: PropositionService,
    private questionService: QuestionService,private activeModal: NgbActiveModal,private examService: ExamService ) {
      this.subscription = this.MessageService.getMessage().subscribe(message => { 
        this.getAll()
        this.message = message;
       });  
     }
    
  ngOnInit() {
    this.getAll()
  }
  getAll(){
    this.propositionService.getPropositionByQcmQuestion(this.route["snapshot"]["params"]["id"],0).subscribe(
      (value) => { 
        this.propositions= value["body"]["content"]
      },
      (error) => {
    })
  }
  openNew(questionId:number) {
    const modalRef = this.modalService.open( NewQcmPropositionComponent, {windowClass:  'fadeStyle' , centered: true});
     modalRef.componentInstance.questionId = this.route["snapshot"]["params"]["id"];
  }
  openEdit(propositionId:number) { 
    const modalRef = this.modalService.open( EditQcmPropositionComponent, {windowClass:  'fadeStyle' , centered: true});
     modalRef.componentInstance.questionId = this.route["snapshot"]["params"]["id"];
     modalRef.componentInstance.propositionId = propositionId;
  }

  openDelete(propositionId:number) { 
    const modalRef = this.modalService.open( DeleteQcmPropositionComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.questionId = this.route["snapshot"]["params"]["id"];
    modalRef.componentInstance.propositionId = propositionId; 
  }
}
