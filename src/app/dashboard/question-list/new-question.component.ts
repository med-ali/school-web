import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Question } from '../models/Question.model';
import { Proposition } from '../models/Proposition.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { QuestionService } from '../services/question.service';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-new-question',
  templateUrl: './new-question.component.html',
  styleUrls: ['./new-question.component.scss']
})
export class NewQuestionComponent implements OnInit {
  questionForm: FormGroup;
  exam:Exam
  examId:number
  questionId:number
  tabPropositions:Proposition[]
  type:string = "qroQuestion"
  constructor(private MessageService: MessageService,private modalService: NgbModal,private formBuilder: FormBuilder,
    private questionService: QuestionService,private activeModal: NgbActiveModal,private examService: ExamService ) { }
  initForm() { 
    this.questionForm = this.formBuilder.group({
      Title: ['', Validators.required],
      Description: ['', Validators.required],
      Response:['' ],
      type: [''],
      score:['', Validators.required],
      propositions: this.formBuilder.array([
        this.initProposition(),
      ])  
    });
  }

  onChange(){
    if (this.type=="qroQuestion")
    {
      this.type="qcmQuestion"
    }else{
      this.type = "qroQuestion"
    }
  }

  addProposition(){
    const control = <FormArray>this.questionForm['controls']['propositions'];
    control.push(this.initProposition());
  }

  removeProposition(i: number) {
    const control = <FormArray>this.questionForm['controls']['propositions'];
    control.removeAt(i);
  }

  initProposition() {
    return this.formBuilder.group({
        description: [''],
        isTrue:[false]
    });
  }
  
  ngOnInit() {
    this.initForm();
    this.examService.findByid(this.examId).subscribe( (value) => { 
      this.exam = value["body"]["exam"] 
    })
  }

  refresh(): void {
    window.location.reload();
  }

  onSubmitForm() {
    const formValue = this.questionForm.value;
    const newQuestion = new Question(
      formValue['Title'],
      formValue['Description'],
      formValue['Response'],
      this.type,
      formValue['score'],
      false
    );
    for (let e of this.questionForm['controls'].propositions["value"]) { 
       const newProposition = new Proposition(
        e['description'],
        e['isTrue'],
        false,
        null, 
        null
      ); 
    } 
    if (this.type  == "qcmQuestion"){
      this.questionService.createQcm(this.examId,newQuestion,this.questionForm['controls'].propositions["value"]).subscribe(
        (value) => { 
          this.clear()
          this.MessageService.sendMessage("question added");
        },
        (error) => {
          this.clear()
          this.openError()
        })
    }
    else{
      this.questionService.createQro(this.examId,newQuestion).subscribe(
        (value) => {  
          this.clear()
          this.MessageService.sendMessage("question deleted")
        },
        (error) => {
          this.clear()
          this.openError()
        })
    }
  }

  
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
