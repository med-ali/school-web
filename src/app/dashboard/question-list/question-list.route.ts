import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { QuestionListComponent  } from './question-list.component'; 
import { EditQcmPropositionComponent } from './edit-qcm-proposition.component';
import { ListQcmPropositionComponent } from './list-qcm-proposition.component';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
export const QuestionRoute: Routes = [
    { 
        path: 'questions/:id/answers',
        component: ListQcmPropositionComponent,canActivate: [AuthGuardService,AdminGuardService]
    } 
];

export const QuestionPopupRoute: Routes = [
     
];