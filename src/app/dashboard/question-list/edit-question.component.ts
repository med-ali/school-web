import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Question } from '../models/Question.model';
import { Proposition } from '../models/Proposition.model';
import { Exam } from '../models/Exam.model';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { AlertComponent } from '../../shared/alert/alert.component';
import { QuestionService } from '../services/question.service';
import { PropositionService } from '../services/proposition.service';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {
  questionForm: FormGroup;
  exam:Exam
  question:Question
  examId:number
  questionId:number
  tabPropositions:Proposition[]
  type:string 
  constructor(private MessageService: MessageService,private modalService: NgbModal,private formBuilder: FormBuilder,private propositionService: PropositionService,
  private questionService: QuestionService,private activeModal: NgbActiveModal,private examService: ExamService ) { }
  
  initForm() { 
    this.questionForm = this.formBuilder.group({
      Title: ['', Validators.required],
      Description: ['', Validators.required],
      Response: ['', Validators.required],
      answers: this.formBuilder.array([
        this.initanswer(),
      ])  
    });
  }

  initanswer() {
    return this.formBuilder.group({
        description: [''],
        isTrue:[false]
    });
  }

  ngOnInit() {
    this.initForm();
    this.questionService.findByid(this.questionId).subscribe( (value) => {  
      this.question = value["body"] 
      this.type = this.question.type
    })
    
    this.propositionService.getPropositionByQcmQuestion(this.questionId,0).subscribe(
      (value) => {  
        this.tabPropositions= value["body"]["content"]
      },
      (error) => {
    })
  }

  onSubmitForm(questionId:number) { 
    const formValue = this.questionForm.value;
    const editQuestion = new Question(
      formValue['Title'],
      formValue['Description'],
      formValue['Response'],
      this.type,
      formValue['score'],
      false
    );
     
    this.questionService.editQuestion(this.questionId,editQuestion).subscribe(
    (value) => { 
      this.clear()
      this.MessageService.sendMessage("question updated");
    },
    (error) => {
      this.clear()
      this.openError()
    })
  }

  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to create!', status: false };
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  refresh(): void {
    window.location.reload();
  }

}
