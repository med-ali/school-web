import { Component, OnInit,ViewChild } from '@angular/core';
import { ExamService } from '../services/exam.service';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-delete-exam-user-registration',
  templateUrl: './delete-exam-user-registration.component.html',
  styleUrls: ['./delete-exam-user-registration.component.scss']
})
export class DeleteExamUserRegistrationComponent implements OnInit {
  @ViewChild(AlertComponent) alert: AlertComponent;
  constructor(private MessageService: MessageService,private modalService: NgbModal,private examService: ExamService,private activeModal: NgbActiveModal) { }
  userId:number;
  examId:number  
  message:Message

  ngOnInit() {
    this.message ={ message: 'ok', status: true };
  }

  refresh(): void {
    window.location.reload();
  }
   
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }

  confirmDelete(userId:number,examId:number){
    this.examService.deleteUserExamRelation(userId,examId).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("candidate deleted")
      },
      (error) => {
        this.clear()
        this.openError()
    })
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
