import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,ActivatedRoute } from '@angular/router';
import { ExamService } from '../services/exam.service';
import { Exam } from '../models/Exam.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-delete-exam',
  templateUrl: './delete-exam.component.html',
  styleUrls: ['./delete-exam.component.scss']
})
export class DeleteExamComponent implements OnInit {
  exam: Exam;
  examId:number
  constructor( private MessageService: MessageService,
      private examService: ExamService,
      public activeModal: NgbActiveModal,
      private modalService: NgbModal
      ,private router: Router
  ) {
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to delete!', status: false };
  }
  ngOnInit() {
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  refresh(): void {
      window.location.reload();
  }

  confirmDelete(id: number) {
    this.examService.delete(id).subscribe(
      (value) => { 
        this.clear()  
        this.MessageService.sendMessage("exam deleted")
      },
      (error) => {
        this.clear()
        this.openError()
      }
    );
  }
}
