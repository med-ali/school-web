import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormArray } from '@angular/forms';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,ActivatedRoute } from '@angular/router';
import { ExamService } from '../services/exam.service';
import { Exam } from '../models/Exam.model';
import { AlertComponent } from '../../shared/alert/alert.component';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-edit-exam',
  templateUrl: './edit-exam.component.html',
  styleUrls: ['./edit-exam.component.scss']
})
export class EditExamComponent implements OnInit {
  examId:number
  examForm: FormGroup;
  constructor(private MessageService: MessageService,private modalService: NgbModal,private formBuilder: FormBuilder,private examService: ExamService,
    private router: Router,private route: ActivatedRoute,private activeModal: NgbActiveModal) {
      
  }

  ngOnInit() {
    this.initForm();
  }
  initForm() { 
    this.examForm = this.formBuilder.group({
      Title: ['', Validators.required],
      Description: ['', Validators.required]  
    });
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to edit !', status: false };
  }
  onSubmitForm(id:number) {
    const formValue = this.examForm.value;
    const newExam = new Exam(
      formValue['Title'],
      formValue['Description']
    );
    this.examService.editExam(id,newExam).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("exam updated")
      },
      (error) => {
        this.clear()
        this.openError()
      })
  }
  refresh(): void {
    window.location.reload();
  }
}
