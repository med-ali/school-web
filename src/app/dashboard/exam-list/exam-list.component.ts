import { Component, OnDestroy, OnInit , ViewChild } from '@angular/core';
import { User } from '../models/User.model';
import { Exam } from '../models/Exam.model';
import { Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { ExamService } from '../services/exam.service';
import { PagerService } from  '../services/pager.service'; 
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { SearchService } from '../services/search.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { NewExamComponent } from './new-exam.component';
import { EditExamComponent } from './edit-exam.component';
import { DeleteExamComponent } from './delete-exam.component';
import { QuestionListComponent } from '../question-list/question-list.component';
import { MessageService } from '../services/message.service';
@Component({ 
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.scss']
})
export class ExamListComponent implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  exams:Exam[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  compTitle:number = 1
  compDescription:number = 1
  subscription: Subscription;
  message: any;
  sortTitle:boolean = true;
  sortDesc:boolean = true;
  constructor(private MessageService: MessageService,private activatedRoute: ActivatedRoute,private _searchService: SearchService,private modalService: NgbModal, private examService: ExamService ,private userService: UserService, private pagerService: PagerService,private route: ActivatedRoute,private router: Router,
    ) {  
      this.subscription = this.MessageService.getMessage().subscribe(message => { 
        this.getAll()
        this.message = message;
       });   
  }
  openNew() {
    const modalRef = this.modalService.open( NewExamComponent, {windowClass:  'fadeStyle' , centered: true});
    // modalRef.componentInstance.id = id;
  }
  
  openViewQuestion(examId:number){
    const modalRef = this.modalService.open( QuestionListComponent, {windowClass:  'fadeStyle' , centered: true});
  }

  openEdit(examId :number) {
    const modalRef = this.modalService.open(EditExamComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.examId = examId;
  }

  openDelete(examId :number) {
    const modalRef = this.modalService.open(DeleteExamComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.examId = examId;
  }

  TabReturn(length){
    return new Array(length);
  }
  
  search(){ 
    if (this.queryField['value']){
      this.examService.findAllByTiyTitle(this.queryField['value'],0).subscribe(
        (value) => {
          this.exams= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
          //this.setPage(1);
        }
      );
    }else{
      this.examService.query(0).subscribe(  
        (value) => {
          this.exams= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  ngOnInit() {
    this.getAll()
  }
  getAll(){
    this.currentPage = 0
    this.examService.query(0).subscribe(  
      (value) => {
        this.exams= value["body"]["content"];
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }else{
          this.pageView =  this.totalPages
        }
        //this.setPage(1);
      }
    );   
  }
/******************Pagination *****************/
setPage(page: number) {
  if ( page < this.totalPages  && 0 <= page   ){   
    if (this.queryField['value']){
      this.currentPage = page
      this.examService.querySearch(this.queryField['value'],page).subscribe(  
        (value) => { 
          this.exams= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
        }
      );
    }
    else{
      this.currentPage = page
      this.examService.query(page).subscribe(  
        (value) => {
          this.exams= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
        }
      );
    }
     
  }  
} 
/****************** Sort *****************/
  sort_by_title(key:string){
    if (this.queryField['value']){
      if (this.compTitle % 2 == 0){
        this.sortTitle = true;
        this.examService.sortAscSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => { 
            console.log(value)
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortTitle = false;
        this.examService.sortDescSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => { 
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    else
    {
      if (this.compTitle % 2 == 0){
        this.sortTitle = true;
        this.examService.sortAsc(key,this.currentPage).subscribe( 
          (value) => { 
            console.log(value)
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortTitle = false;
        this.examService.sortDesc(key,this.currentPage).subscribe( 
          (value) => { 
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compTitle = this.compTitle + 1
  }

  sort_by_description(key:string){
    if (this.queryField['value']){
      if (this.compDescription % 2 == 0){
        this.sortDesc = true;
        this.examService.sortAscSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => { 
            console.log(value)
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortDesc = false;
        this.examService.sortDescSearch(this.queryField['value'],key,this.currentPage).subscribe( 
          (value) => { 
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    else
    {
      if (this.compDescription % 2 == 0){
        this.sortDesc = true;
        this.examService.sortAsc(key,this.currentPage).subscribe( 
          (value) => { 
            console.log(value)
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        );
      }
      else{
        this.sortDesc = false;
        this.examService.sortDesc(key,this.currentPage).subscribe( 
          (value) => { 
            this.exams = value["body"]["content"]; 
            this.totalElements = value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]       
          }
        ); 
      }
    }
    this.compDescription = this.compDescription + 1
  }
}