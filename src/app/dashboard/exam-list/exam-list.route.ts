import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { ExamListComponent  } from './exam-list.component';  
import { ExamDetailComponent } from './exam-detail.component';
import { QuestionListComponent } from '../question-list/question-list.component';
import { AuthGuardService } from '../services/auth.guard.service';
import { AdminGuardService } from '../services/admin.guard.service';
export const ExamRoute: Routes = [
    { 
        path: 'exams',
        component: ExamListComponent,canActivate: [AuthGuardService,AdminGuardService]
    },
    { 
        path: 'exam/detail/:id',
        component: ExamDetailComponent,canActivate: [AuthGuardService,AdminGuardService]
    } ,
    { 
        path: 'detail/questions/:id',
        component: QuestionListComponent,canActivate: [AuthGuardService,AdminGuardService]
    } 
];

export const ExamPopupRoute: Routes = [
     
];