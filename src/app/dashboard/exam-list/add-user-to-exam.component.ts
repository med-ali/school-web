import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SearchService } from '../services/search.service';
import { User } from '../models/User.model';
import { FormControl } from '@angular/forms';
import { Exam } from '../models/Exam.model';
import { ExamUser } from '../models/ExamUser.model';
import { ExamService } from '../services/exam.service';
import { FormBuilder,FormGroup,Validators,FormArray ,ValidatorFn} from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { AlertComponent } from '../../shared/alert/alert.component';
import { Message } from '../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../services/message.service';
@Component({
  selector: 'app-add-user-to-exam',
  templateUrl: './add-user-to-exam.component.html',
  styleUrls: ['./add-user-to-exam.component.scss']
})
export class AddUserToExamComponent implements OnInit {

  constructor(private MessageService: MessageService,private modalService: NgbModal,private router: Router,private activeModal: NgbActiveModal,private examService: ExamService, private formBuilder: FormBuilder,private _searchService: SearchService,private userService: UserService) { }
  id:number
  examId:number
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  userExam:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  UsersChecked : Array<any> = []
  idUsersChecked : Array<any> = []
  TabDate : Array<any> = []
  sessionDate : any
  TabExamUser:ExamUser[]
  exam:Exam
    
  openError() {
    const modalRef = this.modalService.open( AlertComponent, {windowClass:  'fadeStyle' , centered: true});
    modalRef.componentInstance.message= { message: 'Failed to add candidat !', status: false };
  }

  TabReturn(length){
    return new Array(length);
  }

  onChange(id:number, isChecked: boolean) {
    if(isChecked) {
      this.UsersChecked.push(this.findUserById(id));
      this.idUsersChecked.push(id);
    } else {
      let index = this.UsersChecked.indexOf(this.findUserById(id));
      this.UsersChecked.splice(index,1); 
      let idIndex = this.idUsersChecked.indexOf(this.findUserById(id));
      this.idUsersChecked.splice(idIndex,1); 
    } 
  }

  isEmpty(Tab :any[]){
      let resultat:Boolean = false
      for (let e of Tab) {
        if (e===undefined) {
          resultat = true;
          break;
        }
      }
    return resultat;
  }

  onChangeDate(date:any){ 
    console.log(date)
    this.sessionDate = date
     
  }

  submit(examId:number){ 
    this.TabExamUser = []
    console.log(this.sessionDate)
    this.UsersChecked.forEach(user => {
      const newExamUser = new ExamUser(
        user,
        this.exam,
        this.sessionDate
      );   
      this.TabExamUser.push(newExamUser) 
    });
    this.examService.createExamUser(this.TabExamUser ).subscribe(
      (value) => {
        this.clear()  
        this.MessageService.sendMessage("candidate added")
      },
      (error) => {
        this.clear()
        this.openError()
    }) 
  }
  
  findUserById(id:number){ 
    let user:User 
    for (let e of this.users) {
      if (e.id === id) {
        user = e ;
        break;
      }
    }
    return user;  
  }
  
  findUserExamById(id:number){ 
    let resultat:boolean=false 
    for (let e of this.TabExamUser) {
      if (e.users.id === id) {
        resultat = true
        break;
      }
    }
    return resultat;  
  }

  ngOnInit() {
    this.users= []
    this.currentPage = 0 
    this.TabExamUser = []
    this.sessionDate = "2019-07-30"
  }
  getAllUserNoinscrit(){
    this.userService.getAllUsersNoInscrit(this.examId,0).subscribe(  
      (value) => {
        this.users= value["body"]["content"]
        this.totalElements=value["body"]["totalElements"]
        this.totalPages = value["body"]["totalPages"]
        if (this.totalPages>6){
          this.pageView =  6
        }
        else{
          this.pageView =  this.totalPages
        }
      }
    );   
  }
  search(){ 
    if (this.queryField['value']){
      this.users = []
      this.userService.getUsersSearchByLastNameNoInscrit(this.examId,0,this.queryField['value']).subscribe(
        (value) => { 
          this.users= value["body"]["content"];
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          } 
        }
      );
    }else{
      this.users = []
      this.userService.getAllUsersNoInscrit(this.examId,0).subscribe(  
        (value) => {
          this.users= value["body"]["content"]
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }
          else{
            this.pageView =  this.totalPages
          }
        }
      );   
    }
  }
  setPage(page: number) {
    if ( page < this.totalPages && 0 <= page   ){   
      if (this.queryField['value']){
        this.users = []
        this.currentPage = page
        this.userService.getUsersSearchByLastNameNoInscrit(this.examId,page,this.queryField['value']).subscribe(  
          (value) => {
            this.users= value["body"]["content"] 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
            //this.setPage(1);
          }
        );
      }
      else{
        this.currentPage = page
        this.users = []
        this.userService.getAllUsersNoInscrit(this.examId,page).subscribe(  
          (value) => {
            this.users= value["body"]["content"]; 
            this.totalElements=value["body"]["totalElements"]
            this.totalPages = value["body"]["totalPages"]
          }
        );
      }   
    }  
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  refresh(): void {
    window.location.reload();
  }
}
