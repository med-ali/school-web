 
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
//import { ProfilComponent } from './';
import { ExamListComponent  } from './exam-list.component'; 
import {ExamRoute  } from './exam-list.route';
import {ExamPopupRoute } from './exam-list.route';
//import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  CommonModule} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DeleteUserComponent,UserDeletePopupComponent  } from './delete-user.component';
//import {  EditUserComponent } from './edit-user.component';
import {MatDialogModule} from '@angular/material';
import { ExamDetailComponent } from './exam-detail.component';
import { NewExamComponent } from './new-exam.component';
//import {  NewUserComponent } from './new-user.component';
import {MatNativeDateModule,  MatDatepickerModule, NativeDateModule } from '@angular/material';
import { AddUserToExamComponent } from './add-user-to-exam.component';
import { DeleteExamUserRegistrationComponent } from './delete-exam-user-registration.component';
import { EditExamComponent } from './edit-exam.component';
import { DeleteExamComponent } from './delete-exam.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
const ENTITY_STATES = [
    ...ExamRoute,
    ...ExamPopupRoute,
];

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        MatDialogModule,
        MatDatepickerModule,
        NativeDateModule,
        MatNativeDateModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
        
    ],
    declarations: [
        ExamListComponent,
        ExamDetailComponent,
        NewExamComponent,
        AddUserToExamComponent,
        DeleteExamUserRegistrationComponent,
        EditExamComponent,
        DeleteExamComponent
    ],
    entryComponents: [
        NewExamComponent ,
        AddUserToExamComponent,
        DeleteExamUserRegistrationComponent,
        EditExamComponent,
        DeleteExamComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamListModule { }
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}