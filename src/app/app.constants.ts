import { NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";

let _VERSION = '0.0.1-SNAPSHOT';
let _DEBUG_INFO_ENABLED = true;
let _SERVER_API_URL = 'http://54.39.2.101:8080/school/'; 
//let _SERVER_API_URL = 'http://localhost:8080/'; 



export const VERSION = _VERSION;
export const DEBUG_INFO_ENABLED = _DEBUG_INFO_ENABLED;
export const SERVER_API_URL = _SERVER_API_URL;
export const APPLICATION_NAME = "my-school";
export const ADMIN = "ROLE_ADMIN";
export const USER = "ROLE_USER";


export const NGB_MODAL_OPTIONS: NgbModalOptions = {
  backdrop: 'static',
  keyboard: false
};
export const NGB_MODAL_OPTIONS_LG: NgbModalOptions = {
  backdrop: 'static',
  keyboard: false,
  size: 'lg'
};

export const NGB_MODAL_OPTIONS_XL: NgbModalOptions = {
  backdrop: 'static',
  keyboard: false,
  size: 'lg',
  windowClass: 'modal-xxl' 
};
